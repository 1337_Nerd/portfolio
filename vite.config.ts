import { sveltekit } from '@sveltejs/kit/vite'
import { FontaineTransform } from 'fontaine'
import { defineConfig } from 'vite'
import { imagetools } from 'vite-imagetools'

export default defineConfig({
	build: {
		assetsInlineLimit: 0,
	},
	plugins: [
		imagetools(),
		sveltekit(),
		FontaineTransform.vite({
			fallbacks: ['-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial'],
			resolvePath: (id) => new URL(`.${id}`, import.meta.url),
		})
	]
});
