import { type Handle, redirect } from '@sveltejs/kit'

export const handle: Handle = async ({ event, resolve }) => {
	return resolve(event, {
		preload: ({path}) => path.includes('Brandon-Grotesque-RegularItalic-400') || path.includes('Brandon-Grotesque-BoldItalic-700')
	})
}