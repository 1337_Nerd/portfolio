import { describe, it, expect } from 'vitest'
import generateChances, { end, getRate, statusLookup } from './Gen2'

describe('Gen 2 catch rate calculator', () => {
    const bulbasaurInfo = { capture_rate: 45, pokemon_v2_pokemons: [{ weight: 69, pokemon_v2_pokemonstats: [{ base_stat: 45, pokemon_v2_stat: { name: 'hp' } }, { base_stat: 45, pokemon_v2_stat: { name: 'speed' } }], pokemon_v2_pokemontypes: [{ pokemon_v2_type: { name: 'grass' } }, { pokemon_v2_type: { name: 'poison' } }]}]}
    it('should calculate correct concise catch rates for all balls', () => {
        expect(generateChances(1, bulbasaurInfo, 'All', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual([{ "ball": "Poké Ball", "breakdown": 6.25 }, { "ball": "Great Ball", "breakdown": 8.984375 }, { "ball": "Ultra Ball", "breakdown": 12.109375 }, { "ball": "Master Ball", "breakdown": 100 }, { "ball": "Fast Ball", "breakdown": 6.25 }, { "ball": "Level Ball", "breakdown": 17.96875 }, { "ball": "Lure Ball", "breakdown": 6.25 }, { "ball": "Heavy Ball", "breakdown": 3.515625 }, { "ball": "Love Ball", "breakdown": 6.25 }, { "ball": "Friend Ball", "breakdown": 6.25 }, { "ball": "Moon Ball", "breakdown": 6.25 }, { "ball": "Park Ball", "breakdown": 8.984375 }])
    })
    
    it('should calculate correct catch rates for pokeball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Poké Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.0625), "overall": 6.25 })
    })
    
    it('should calculate correct catch rates for great ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Great Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.08984375), "overall": 8.984375 })
    })
    
    it('should calculate correct catch rates for ultra ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Ultra Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.12109375), "overall": 12.109375 })
    })
    
    it('should calculate correct catch rates for master ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Master Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(1), "overall": 100 })
    })
    
    it('should calculate correct catch rates for fast ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Fast Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.0625), "overall": 6.25 })
    })
    
    it('should calculate correct catch rates for level ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Level Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.1796875), "overall": 17.96875 })
    })

    it('should calculate correct catch rates for lure ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Lure Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.0625), "overall": 6.25 })
    })

    it('should calculate correct catch rates for heavy ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Heavy Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.03515625), "overall": 3.515625 })
    })

    it('should calculate correct catch rates for love ball', () => {
        expect(generateChances(1, bulbasaurInfo, 'Love Ball', 'None', 1, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": new Array(16).fill(0.0625), "overall": 6.25 })
    })

    const lugiaInfo = {"capture_rate":3,"pokemon_v2_pokemons":[{"weight":2160,"pokemon_v2_pokemonstats":[{"base_stat":106,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":110,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"psychic"}},{"pokemon_v2_type":{"name":"flying"}}]}]}
    it('should handle the case of hpmax being greater than 255', () => {
        expect(generateChances(1, lugiaInfo, 'Poké Ball', 'None', 100, 100, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toStrictEqual({ "breakdown": [0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0.0078125, 0, 0.5078125, 0.33984375, 0.20703125, 0.171875, 0.1328125], "overall": 8.984375 })
    })
})

describe('Gen 2 status rates', () => {
    it('should return 10 if asleep or frozen', () => {
        expect(statusLookup('Sleep')).toBe(10)
        expect(statusLookup('Freeze')).toBe(10)
    })
    it('should return 0 otherwise', () => {
        expect(statusLookup('None')).toBe(0)
    })
})

describe('Gen 2 catch rates', () => {
    describe('Great ball', () => {
        it('should multiply by 1.5 for', () => {
            expect(getRate(69, 45, 'Great Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 1.5)
        })
    })

    describe('Park Ball', () => {
        it('should multiply by 1.65', () => {
            expect(getRate(69, 45, 'Park Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 1.5)
        })
    })

    describe('Ultra Ball', () => {
        it('should multiply by 2', () => {
            expect(getRate(69, 45, 'Ultra Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 2)
        })
    })

    describe('Fast Ball', () => {
        const ball = 'Fast Ball'
        it('should not multiply for Bulbasaur', () => {
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45)
        })

        it('should multiply for Grimer', () => {
            expect(getRate(69, 45, ball, 81, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 4)
        })

        it('should multiply for Tangela', () => {
            expect(getRate(69, 45, ball, 88, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 4)
        })

        it('should multiply for Magnemite', () => {
            expect(getRate(69, 45, ball, 114, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 4)
        })
    })

    describe('Level Ball', () => {
        const ball = 'Level Ball'
        it('should return regular rate if your level is the same as the enemy', () => {
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45)
        })

        it('should return 2x your rate if your level is twice the enemy', () => {
            expect(getRate(69, 45, ball, 1, 2, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 2)
        })

        it('should return 4x your rate if your level is 4x the enemy', () => {
            expect(getRate(69, 45, ball, 1, 4, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 * 4)
        })

        it('should return 8x your rate if your level is any higher than the enemy', () => {
            expect(getRate(69, 30, ball, 1, 8, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(30 * 8)
        })
    })

    describe('Lure Ball', () => {
        const ball = 'Lure Ball'
        it('should return 3x your rate if fishing, 1x otherwise', () => {
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', true, false, 'Male', 'Male')).toBe(45 * 3)
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45)
        })
    })

    describe('Heavy Ball', () => {
        it('should return +40 if game is crystal and dex is a multiple of 64', () => {
            for (let i = 64; i < end; i += 64) {
                expect(getRate(69, 45, 'Heavy Ball', i, 1, 1, 'Crystal', false, false, 'Male', 'Male')).toBe(45 + 40)
                expect(getRate(69, 45, 'Heavy Ball', i, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 - 20)
            }
        })

        it('should return +40 if weight is at least 4096', () => {
            expect(getRate(4096, 45, 'Heavy Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 + 40)
        })

        it('should return +30 if weight is at least 3072',() => {
            expect(getRate(3072, 45, 'Heavy Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 + 30)
        })

        it('should return +20 if weight is at least 2048',() => {
            expect(getRate(2048, 45, 'Heavy Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 + 20)
        })

        it('should return rate if weight is at least 1024',() => {
            expect(getRate(1024, 45, 'Heavy Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45)
        })

        it('should return -20 if weight is below 1024',() => {
            expect(getRate(1023, 45, 'Heavy Ball', 1, 1, 1, 'Gold/Silver', false, false, 'Male', 'Male')).toBe(45 - 20)
        })
    })

    describe('Love Ball', () => {
        const ball = 'Love Ball'
        it('should return 8x if genders are the same', () => {
            expect(getRate(69, 30, ball, 1, 1, 1, 'Gold/Silver', false, true, 'Male', 'Male')).toBe(30 * 8)
        })

        it ('should not return 8x if genders are both genderless', () => {
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', false, true, 'Genderless', 'Genderless')).toBe(45)
        })

        it ('should not return 8x if genders are different', () => {
            expect(getRate(69, 45, ball, 1, 1, 1, 'Gold/Silver', false, true, 'Male', 'Female')).toBe(45)
        })
    })

    describe('Caps at 255', () => {
        it ('should cap at 255', () => {
            expect(getRate(69, 256, 'Poké Ball', 1, 1, 1, 'Gold/Silver', false, true, 'Male', 'Female')).toBe(255)
        })
    })

})