import { describe, it, expect } from 'vitest'
import generateChances, { getApricorn, getRate, statusLookup } from './Gen3'

describe('Gen 3 catch rate calculator', () => {
	const bulbasaurInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":69,"pokemon_v2_pokemonstats":[{"base_stat":45,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":45,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}]}]}
	const bayleefInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":158,"pokemon_v2_pokemonstats":[{"base_stat":60,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":60,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"grass"}}]}]}
	const shedinjaInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":12,"pokemon_v2_pokemonstats":[{"base_stat":1,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":40,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"bug"}},{"pokemon_v2_type":{"name":"ghost"}}]}]}
	const rayquazaInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":2065,"pokemon_v2_pokemonstats":[{"base_stat":105,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":95,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"dragon"}},{"pokemon_v2_type":{"name":"flying"}}]}]}
	const rattataInfo = {"capture_rate":255,"pokemon_v2_pokemons":[{"weight":35,"pokemon_v2_pokemonstats":[{"base_stat":30,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":72,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"normal"}}]}]}

	it('should calculate correct concise catch rates for all balls', () => {
		expect(generateChances(1, bulbasaurInfo, 'No', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":6.249237095470761},{"ball":"Great Ball","breakdown":9.265094098269621},{"ball":"Ultra Ball","breakdown":12.330296372254605},{"ball":"Safari Ball","breakdown":9.265094098269621},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":6.249237095470761},{"ball":"Net Ball","breakdown":6.249237095470761},{"ball":"Nest Ball","breakdown":23.416422290071665},{"ball":"Dive Ball","breakdown":6.249237095470761},{"ball":"Repeat Ball","breakdown":6.249237095470761},{"ball":"Timer Ball","breakdown":6.249237095470761},{"ball":"Luxury Ball","breakdown":6.249237095470761},{"ball":"Heal Ball","breakdown":6.249237095470761},{"ball":"Quick Ball","breakdown":27.972602799249046},{"ball":"Dusk Ball","breakdown":6.249237095470761},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":6.249237095470761},{"ball":"Sport Ball","breakdown":9.265094098269621},{"ball":"Friend Ball","breakdown":6.249237095470761},{"ball":"Fast Ball","breakdown":6.249237095470761},{"ball":"Heavy Ball","breakdown":3.1425048836545297},{"ball":"Level Ball","breakdown":6.249237095470761},{"ball":"Love Ball","breakdown":6.249237095470761},{"ball":"Lure Ball","breakdown":6.249237095470761},{"ball":"Moon Ball","breakdown":6.249237095470761}])
	})

	it('should calculate correct concise catch rates for a different pokemon', () => {
		expect(generateChances(153, bayleefInfo, 'No', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":6.249237095470761},{"ball":"Great Ball","breakdown":9.265094098269621},{"ball":"Ultra Ball","breakdown":12.330296372254605},{"ball":"Safari Ball","breakdown":9.265094098269621},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":6.249237095470761},{"ball":"Net Ball","breakdown":6.249237095470761},{"ball":"Nest Ball","breakdown":23.416422290071665},{"ball":"Dive Ball","breakdown":6.249237095470761},{"ball":"Repeat Ball","breakdown":6.249237095470761},{"ball":"Timer Ball","breakdown":6.249237095470761},{"ball":"Luxury Ball","breakdown":6.249237095470761},{"ball":"Heal Ball","breakdown":6.249237095470761},{"ball":"Quick Ball","breakdown":27.972602799249046},{"ball":"Dusk Ball","breakdown":6.249237095470761},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":6.249237095470761},{"ball":"Sport Ball","breakdown":9.265094098269621},{"ball":"Friend Ball","breakdown":6.249237095470761},{"ball":"Fast Ball","breakdown":6.249237095470761},{"ball":"Heavy Ball","breakdown":3.1425048836545297},{"ball":"Level Ball","breakdown":6.249237095470761},{"ball":"Love Ball","breakdown":6.249237095470761},{"ball":"Lure Ball","breakdown":6.249237095470761},{"ball":"Moon Ball","breakdown":6.249237095470761}])
	})

	it('should modify the catch rate if shadow pokemon', () => {
		expect(generateChances(153, bayleefInfo, 'Yes (Colosseum)', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":27.972602799249046},{"ball":"Great Ball","breakdown":40.95750005721986},{"ball":"Ultra Ball","breakdown":50.283722645761145},{"ball":"Safari Ball","breakdown":40.95750005721986},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":27.972602799249046},{"ball":"Net Ball","breakdown":27.972602799249046},{"ball":"Nest Ball","breakdown":99.99389662407197},{"ball":"Dive Ball","breakdown":27.972602799249046},{"ball":"Repeat Ball","breakdown":27.972602799249046},{"ball":"Timer Ball","breakdown":27.972602799249046},{"ball":"Luxury Ball","breakdown":27.972602799249046},{"ball":"Heal Ball","breakdown":27.972602799249046},{"ball":"Quick Ball","breakdown":99.99389662407197},{"ball":"Dusk Ball","breakdown":27.972602799249046},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":27.972602799249046},{"ball":"Sport Ball","breakdown":40.95750005721986},{"ball":"Friend Ball","breakdown":27.972602799249046},{"ball":"Fast Ball","breakdown":27.972602799249046},{"ball":"Heavy Ball","breakdown":23.416422290071665},{"ball":"Level Ball","breakdown":27.972602799249046},{"ball":"Love Ball","breakdown":27.972602799249046},{"ball":"Lure Ball","breakdown":27.972602799249046},{"ball":"Moon Ball","breakdown":27.972602799249046}])
	})

	it('should not modify the catch rate if not different in colosseum', () => {
		expect(generateChances(1, bulbasaurInfo, 'Yes (Colosseum)', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":6.249237095470761},{"ball":"Great Ball","breakdown":9.265094098269621},{"ball":"Ultra Ball","breakdown":12.330296372254605},{"ball":"Safari Ball","breakdown":9.265094098269621},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":6.249237095470761},{"ball":"Net Ball","breakdown":6.249237095470761},{"ball":"Nest Ball","breakdown":23.416422290071665},{"ball":"Dive Ball","breakdown":6.249237095470761},{"ball":"Repeat Ball","breakdown":6.249237095470761},{"ball":"Timer Ball","breakdown":6.249237095470761},{"ball":"Luxury Ball","breakdown":6.249237095470761},{"ball":"Heal Ball","breakdown":6.249237095470761},{"ball":"Quick Ball","breakdown":27.972602799249046},{"ball":"Dusk Ball","breakdown":6.249237095470761},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":6.249237095470761},{"ball":"Sport Ball","breakdown":9.265094098269621},{"ball":"Friend Ball","breakdown":6.249237095470761},{"ball":"Fast Ball","breakdown":6.249237095470761},{"ball":"Heavy Ball","breakdown":3.1425048836545297},{"ball":"Level Ball","breakdown":6.249237095470761},{"ball":"Love Ball","breakdown":6.249237095470761},{"ball":"Lure Ball","breakdown":6.249237095470761},{"ball":"Moon Ball","breakdown":6.249237095470761}])
	})

	it('should modify the catch rate differently for pokemon xd', () => {
		expect(generateChances(82, bayleefInfo, 'Yes (XD)', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":14.340031024187144},{"ball":"Great Ball","breakdown":23.416422290071665},{"ball":"Ultra Ball","breakdown":33.694679800861685},{"ball":"Safari Ball","breakdown":23.416422290071665},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":14.340031024187144},{"ball":"Net Ball","breakdown":14.340031024187144},{"ball":"Nest Ball","breakdown":62.42426856909149},{"ball":"Dive Ball","breakdown":14.340031024187144},{"ball":"Repeat Ball","breakdown":14.340031024187144},{"ball":"Timer Ball","breakdown":14.340031024187144},{"ball":"Luxury Ball","breakdown":14.340031024187144},{"ball":"Heal Ball","breakdown":14.340031024187144},{"ball":"Quick Ball","breakdown":62.42426856909149},{"ball":"Dusk Ball","breakdown":14.340031024187144},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":14.340031024187144},{"ball":"Sport Ball","breakdown":23.416422290071665},{"ball":"Friend Ball","breakdown":14.340031024187144},{"ball":"Fast Ball","breakdown":14.340031024187144},{"ball":"Heavy Ball","breakdown":12.330296372254605},{"ball":"Level Ball","breakdown":14.340031024187144},{"ball":"Love Ball","breakdown":14.340031024187144},{"ball":"Lure Ball","breakdown":14.340031024187144},{"ball":"Moon Ball","breakdown":14.340031024187144}])
	})

	it('should not modify the catch rate if not different in xd', () => {
		expect(generateChances(1, bulbasaurInfo, 'Yes (XD)', 'All', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":6.249237095470761},{"ball":"Great Ball","breakdown":9.265094098269621},{"ball":"Ultra Ball","breakdown":12.330296372254605},{"ball":"Safari Ball","breakdown":9.265094098269621},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":6.249237095470761},{"ball":"Net Ball","breakdown":6.249237095470761},{"ball":"Nest Ball","breakdown":23.416422290071665},{"ball":"Dive Ball","breakdown":6.249237095470761},{"ball":"Repeat Ball","breakdown":6.249237095470761},{"ball":"Timer Ball","breakdown":6.249237095470761},{"ball":"Luxury Ball","breakdown":6.249237095470761},{"ball":"Heal Ball","breakdown":6.249237095470761},{"ball":"Quick Ball","breakdown":27.972602799249046},{"ball":"Dusk Ball","breakdown":6.249237095470761},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":6.249237095470761},{"ball":"Sport Ball","breakdown":9.265094098269621},{"ball":"Friend Ball","breakdown":6.249237095470761},{"ball":"Fast Ball","breakdown":6.249237095470761},{"ball":"Heavy Ball","breakdown":3.1425048836545297},{"ball":"Level Ball","breakdown":6.249237095470761},{"ball":"Love Ball","breakdown":6.249237095470761},{"ball":"Lure Ball","breakdown":6.249237095470761},{"ball":"Moon Ball","breakdown":6.249237095470761}])
	})

	it('should calculate level difference correctly', () => {
		// Small imprecision when getting average. *Technically*, this is incorrect but the user will never see the number at this precision
		expect(generateChances(1, bulbasaurInfo, 'No', 'Level Ball', 100, 1, 'None', 20, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual({ "breakdown": new Array(32).fill(0.3369467980086171), "overall": 33.694679800861685 })
	})

	it('should calculate hp differently for shedinja', () => {
		expect(generateChances(292, shedinjaInfo, 'No', 'Poké Ball', 100, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual({ "breakdown": new Array(32).fill(0.06249237095470761), "overall": 6.249237095470761 })
		expect(generateChances(292, shedinjaInfo, 'No', 'Poké Ball', 1, 1, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual({ "breakdown": new Array(32).fill(0.06249237095470761), "overall": 6.249237095470761 })
	})

	it('should handle hp being higher than 255', () => {
		expect(generateChances(384, rayquazaInfo, 'No', 'All', 100, 100, 'None', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual([{"ball":"Poké Ball","breakdown":0.41591705588126},{"ball":"Great Ball","breakdown":0.41591705588126},{"ball":"Ultra Ball","breakdown":0.8304943666424203},{"ball":"Safari Ball","breakdown":0.41591705588126},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":0.41591705588126},{"ball":"Net Ball","breakdown":0.41591705588126},{"ball":"Nest Ball","breakdown":0.41591705588126},{"ball":"Dive Ball","breakdown":0.41591705588126},{"ball":"Repeat Ball","breakdown":0.41591705588126},{"ball":"Timer Ball","breakdown":0.41591705588126},{"ball":"Luxury Ball","breakdown":0.41591705588126},{"ball":"Heal Ball","breakdown":0.41591705588126},{"ball":"Quick Ball","breakdown":1.598006409844198},{"ball":"Dusk Ball","breakdown":0.41591705588126},{"ball":"Park Ball","breakdown":100},{"ball":"Cherish Ball","breakdown":0.41591705588126},{"ball":"Sport Ball","breakdown":0.41591705588126},{"ball":"Friend Ball","breakdown":0.41591705588126},{"ball":"Fast Ball","breakdown":0.41591705588126},{"ball":"Heavy Ball","breakdown":2.8325987208270926},{"ball":"Level Ball","breakdown":0.41591705588126},{"ball":"Love Ball","breakdown":0.41591705588126},{"ball":"Lure Ball","breakdown":0.41591705588126},{"ball":"Moon Ball","breakdown":0.41591705588126}])
	})

	it('should handle x going above 255', () => {
		expect(generateChances(19, rattataInfo, 'No', 'Poké Ball', 1, 1, 'Freeze', 1, false, 'Male', 'Male', false, false, 1, false)).toStrictEqual({ 'breakdown': new Array(32).fill(1), 'overall': 100 })
	})
})

describe('Gen 3 status rates', () => {
	it('should return 2 if asleep or frozen', () => {
		expect(statusLookup('Sleep')).toBe(2)
		expect(statusLookup('Freeze')).toBe(2)
	})

	it('should return 1.5 if poisoned, burned, or paralyzed', () => {
		expect(statusLookup('Poisoned')).toBe(1.5)
		expect(statusLookup('Burned')).toBe(1.5)
		expect(statusLookup('Paralyzed')).toBe(1.5)
	})

	it('should return 1 otherwise', () => {
		expect(statusLookup('None')).toBe(1)
	})
})

describe('Gen 3 apricorn rates', () => {
	describe('Fast Ball', () => {
		const ball = 'Fast Ball'
		it('should return 4x if speed is at least 100', () => {
			expect(getApricorn(1, 69, 45, 100, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 * 4)
		})
		it('should return rate otherwise', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45)
		})
	})
	describe('Heavy Ball', () => {
		const ball = 'Heavy Ball'
		it('should return +40 if weight is at least 4096', () => {
			expect(getApricorn(1, 4096, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 + 40)
		})
		it('should return +30 if weight is at least 3072', () => {
			expect(getApricorn(1, 3072, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 + 30)
		})
		it('should return +20 if weight is at least 2048', () => {
			expect(getApricorn(1, 2048, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 + 20)
		})
		it('should return -20 otherwise', () => {
			expect(getApricorn(1, 2047, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 - 20)
		})
	})
	describe('Level Ball', () => {
		const ball = 'Level Ball'
		it('should return 8x if levels are 4x greater', () => {
			expect(getApricorn(1, 69, 30, 45, ball, 20, 1, false, 'Male', 'Male', false)).toBe(30 * 8)
		})
		it('should return 4x if levels are 2x greater', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 5, 1, false, 'Male', 'Male', false)).toBe(45 * 4)
		})
		it('should return 2x if levels are greater', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 2, 1, false, 'Male', 'Male', false)).toBe(45 * 2)
		})
		it('should return rate otherwise', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45)
			expect(getApricorn(1, 69, 45, 45, ball, 1, 6, false, 'Male', 'Male', false)).toBe(45)
		})
	})
	describe('Love Ball', () => {
		const ball = 'Love Ball'	
		it('should return 8x if genders are the different', () => {
			expect(getApricorn(1, 69, 30, 45, ball, 1, 1, true, 'Male', 'Female', false)).toBe(30 * 8)
		})
		it('should return rate otherwise', () => {
			expect(getApricorn(1, 69, 30, 45, ball, 1, 1, true, 'Male', 'Male', false)).toBe(30)
		})
		it('should return rate if either are genderless', () => {
			expect(getApricorn(1, 69, 30, 45, ball, 1, 1, true, 'Male', 'Genderless', false)).toBe(30)
			expect(getApricorn(1, 69, 30, 45, ball, 1, 1, true, 'Genderless', 'Male', false)).toBe(30)
		})
	})
	describe('Lure Ball', () => {
		const ball = 'Lure Ball'
		it('should return 3x if fishing', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', true)).toBe(45 * 3)
		})
		it('should return rate otherwise', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45)
		})
	})
	describe('Moon Ball', () => {
		const ball = 'Moon Ball'
		it('should return 4x if pokemon evolves using moon stone', () => {
			expect(getApricorn(29, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 * 4)
			expect(getApricorn(300, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45 * 4)
		})
		it('should return rate otherwise', () => {
			expect(getApricorn(1, 69, 45, 45, ball, 1, 1, false, 'Male', 'Male', false)).toBe(45)
		})
	})
})

describe('Gen 3 get rate', () => {
	describe('Great Ball', () => {
		const ball = 'Great Ball'
		it('should return 1.5', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1.5)
		})
	})
	describe('Safari Ball', () => {
		const ball = 'Safari Ball'
		it('should return 1.5', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1.5)
		})
	})
	describe('Sport Ball', () => {
		const ball = 'Sport Ball'
		it('should return 1.5', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1.5)
		})
	})
	describe('Ultra Ball', () => {
		const ball = 'Ultra Ball'
		it('should return 2', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(2)
		})
	})
	describe('Net Ball', () => {
		const ball = 'Net Ball'
		it('should return 3 if bug or water', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"bug"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(3)
			expect(getRate([{"pokemon_v2_type":{"name":"water"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(3)
		})
		it('should return 1 otherwise', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Nest Ball', () => {
		const ball = 'Nest Ball'
		it('should return 3 at level 10', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 10, false, false, 1, false)).toBe(3)
		})
		it('should return 2 at level 20', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 20, false, false, 1, false)).toBe(2)
		})
		it('should return 1 at level 30', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 30, false, false, 1, false)).toBe(1)
		})
		it('should return 1 at any level higher', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 50, false, false, 1, false)).toBe(1)
		})
	})
	describe('Dive Ball', () => {
		const ball = 'Dive Ball'
		it('should return 3.5 if fishing', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, true, false, 1, false)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Repeat Ball', () => {
		const ball = 'Repeat Ball'
		it('should return 3.5 if caught before', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, true, 1, false)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Timer Ball', () => {
		const ball = 'Timer Ball'
		it('should return 1 at turn 1', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
		it('should return 2 at turn 11', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 11, false)).toBe(2)
		})
		it('should return 3 at turn 21', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 21, false)).toBe(3)
		})
		it('should return 4 at turn 31', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 31, false)).toBe(4)
		})
		it('should return 4 at any higher', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 50, false)).toBe(4)
		})
	})
	describe('Quick Ball', () => {
		const ball = 'Quick Ball'
		it('should return 4 on turn 1', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(4)
		})
		it('should return 1 otherwise', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 4, false)).toBe(1)
		})
	})
	describe('Dusk Ball', () => {
		const ball = 'Dusk Ball'
		it('should return 3.5 if dark', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, true)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(getRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
})