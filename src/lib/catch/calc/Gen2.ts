const changesRate: Record<number, number> = {
	20: 90
}
const changesHp: Record<number, number> = {
	219: 50,
	222: 55,
	226: 65
}
export const ballList: string[] = [
	'Poké Ball',
	'Great Ball',
	'Ultra Ball',
	'Master Ball',
	'Fast Ball',
	'Level Ball',
	'Lure Ball',
	'Heavy Ball',
	'Love Ball',
	'Friend Ball',
	'Moon Ball',
	'Park Ball',
	'All'
]
export const statusList: string[] = ['None', 'Sleep', 'Freeze']
export const end = 251

function generateChances(dex: number, info: catchInfo, ball: string, chosenStatus: string, level: number, hp: number, yourLevel: number, gameSelector: string, fishing: boolean, playerPokemon: boolean, wildGender: string, yourGender: string) {
	const catchRate = changesRate[dex] ?? info.capture_rate
	const weight = info.pokemon_v2_pokemons[0].weight
	interface pokemonStat {
		base_stat: number,
		pokemon_v2_stat: {
			name: string
		}
	}
	const hpMax: number = changesHp[dex] ?? info.pokemon_v2_pokemons[0].pokemon_v2_pokemonstats.find((stat: pokemonStat) => stat.pokemon_v2_stat.name === 'hp')?.base_stat
	if (ball !== 'All') {
		const rateModified = getRate(weight, catchRate, ball, dex, yourLevel, level, gameSelector, fishing, playerPokemon, wildGender, yourGender)
		const chances = getChances(rateModified, hp, hpMax, ball, chosenStatus, level)
		if (ball === 'Level Ball')
			return { overall: Math.min((rateModified + 1) / 256, 1) * 100, breakdown: chances }
		return { overall: chances.reduce((a, b) => {return Math.abs(a) + Math.abs(b)}, 0) / chances.length * 100, breakdown: chances }
	}
	const overall: {ball: string, breakdown: number}[] = []
	ballList.forEach((ball) => {
		if (ball === 'All')
			return
		const rateModified = getRate(weight, catchRate, ball, dex, yourLevel, level, gameSelector, fishing, playerPokemon, wildGender, yourGender)
		const breakdown = getChances(rateModified, hp, hpMax, ball, chosenStatus, level)
		let totalChances
		if (ball === 'Level Ball')
			totalChances =  Math.min((rateModified + 1) / 256, 1) * 100
		else
			totalChances =  breakdown.reduce((a, b) => {return Math.abs(a) + Math.abs(b)}, 0) / breakdown.length * 100
		overall.push({ball, breakdown: totalChances})
	})
	return overall
}
function getChances(rateModified: number, hp: number, maxHp: number, ballName: string, chosenStatus: string, level: number): number[] {
	if (ballName === 'Level Ball')
		return new Array(16).fill(Math.min((rateModified + 1) / 256, 1))
	if (ballName === 'Master Ball')
		return new Array(16).fill(1)
	return Array.from({length: 16}, (_, i) => {
		let hpMax = Math.floor((((maxHp + i) * 2 * level) / 100) + level + 10)
		let hpCurr = Math.max(hp / 100, 0.01) * (hpMax << 1)
		hpMax += hpMax << 1
		if (hpMax > 255) {
			hpMax = (hpMax >> 2) % 256
			hpCurr = Math.max(hpCurr >> 2, 1) % 256
		}
		if (hpMax === 0)
			return 0
		return Math.min(Math.floor(Math.max(((hpMax - hpCurr + 256) % 256) * rateModified / hpMax, 1)) % 256 + statusLookup(chosenStatus) + 1, 255) / 256
	})
}
export function getRate(weight: number, rate: number, ballName: string, dex: number, yourLevel: number, level: number, gameSelector: string, fishing: boolean, playerPokemon: boolean, wildGender: string, yourGender: string): number {
	const ballLookup: {[key: string]: () => number} = {
		'Great Ball': () => rate * 1.5,
		'Park Ball': () => rate * 1.5,
		'Ultra Ball': () => rate * 2,
		'Fast Ball': () => {
			if ([81, 88, 114].includes(dex))
				return rate * 4
			return rate
		},
		'Level Ball': () => {
			if (yourLevel <= level)
				return rate
			if (yourLevel >> 1 <= level)	
				return rate * 2
			if (yourLevel >> 2 <= level)
				return rate * 4
			return rate * 8
		},
		'Lure Ball': () => {
			if (fishing)
				return rate * 3
			return rate
		},
		'Heavy Ball': () => {
			if ((dex % 64 === 0 && gameSelector === 'Crystal') || (weight >= 4096))
				return rate + 40
			if (weight >= 3072)
				return rate + 30
			if (weight >= 2048)
				return rate + 20
			if (weight >= 1024)
				return rate
			return rate - 20
		},
		'Love Ball': () => {
			if (playerPokemon && wildGender === yourGender && wildGender !== 'Genderless')
				return rate * 8
			return rate
		}
	}
	const ballFunction = ballLookup[ballName] || (() => rate)
	return Math.min(ballFunction(), 255)
}
export function statusLookup(chosenStatus: string) {
	return ['Sleep', 'Freeze'].includes(chosenStatus) ? 10 : 0
}
export default generateChances