const changesRate: Record<number, number> = {
	550: 25,
	382: 5,
	383: 5,
	483: 30,
	484: 30,
	643: 45,
	644: 45
}
const changesHp: Record<number, number> = {
	219: 50,
	222: 55,
	226: 65,
	337: 70,
	338: 70,
	358: 65,
	527: 55
}
export const ballList: string[] = [
	'Poké Ball',
	'Great Ball',
	'Ultra Ball',
	'Master Ball',
	'Premier Ball',
	'Net Ball',
	'Nest Ball',
	'Dive Ball',
	'Repeat Ball',
	'Timer Ball',
	'Luxury Ball',
	'Heal Ball',
	'Quick Ball',
	'Dusk Ball',
	'Cherish Ball',
	'Dream Ball',
	'All'
]
export const statusList: string[] = ['None', 'Sleep', 'Freeze', 'Poisoned', 'Burned', 'Paralyzed']
export const end = 649

function generateChances(dex: number, info: catchInfo, ball: string, hp: number, chosenStatus: string, grass: boolean, level: number, water: boolean, turn: number, dark: boolean, caught: boolean, entra: string, entree: boolean, totalCaught: number) {
	const catchRate = changesRate[dex] ?? info.capture_rate
	const types: {pokemon_v2_type: {name: string}}[] = info.pokemon_v2_pokemons[0].pokemon_v2_pokemontypes
	interface pokemonStat {
		base_stat: number,
		pokemon_v2_stat: {
			name: string
		}
	}
	const hpMax: number = changesHp[dex] ?? info.pokemon_v2_pokemons[0].pokemon_v2_pokemonstats.find((stat: pokemonStat) => stat.pokemon_v2_stat.name === 'hp')?.base_stat
	if (ball !== 'All') {
		const chances = getRate(catchRate, hp, hpMax, types, ball, level, chosenStatus, water, caught, entra, turn, dark, entree, totalCaught, grass)
		return { overall: chances.reduce((a, b) => {return a + b}, 0) / chances.length * 100, breakdown: chances }
	}
	const overall: {ball: string, breakdown: number}[] = []
	ballList.forEach((ball) => {
		if (ball === 'All')
			return
		const chances = getRate(catchRate, hp, hpMax, types, ball, level, chosenStatus, water, caught, entra, turn, dark, entree, totalCaught, grass)
		overall.push({ball, breakdown: chances.reduce((a, b) => {return Math.abs(a) + Math.abs(b)}, 0) / chances.length * 100})
	})
	return overall
}

export function getRate(catchRate: number, hp: number, hpMax: number, types: {pokemon_v2_type: {name: string}}[], ballName: string, level: number, chosenStatus: string, water: boolean, caught: boolean, entra: string, turns: number, dark: boolean, entree: boolean, totalCaught: number, grass: boolean) {
	if (['Master Ball'].includes(ballName) || entree)
		return new Array(32).fill(1)
	const grassMod = grass ? getGrassMod(totalCaught) : 1
	const entraPower = getEntraMod(entra)
	const caughtMod = getCaughtMod(totalCaught)
	return Array.from({length: 32}, (_, i) => {
		let hpmax = hpMax === 1 ? 1 : Math.floor((((2 * hpMax + i) * level) / 100) + level + 10)
		const hpcurr = Math.max(Math.round(hpmax * hp / 100), 1) * 2
		hpmax += hpmax * 2
		const x = roundDown(round(roundDown((round(round(hpmax - hpcurr) * grassMod) * catchRate * genFiveRate(types, ballName, level, water, caught, turns, dark) / (hpmax))) * statusLookup(chosenStatus)) * (entraPower / 100))
		const cc = (Math.min(255, x) * caughtMod) / 6
		const y = 65536 / (Math.sqrt(Math.sqrt(255 / x)))
		return Math.min((cc / 256) * (y / 65536) + (1 - cc / 256) * Math.pow(y / 65536, 3), 1)
	})
}

export function genFiveRate(types: {pokemon_v2_type: {name: string}}[], ballName: string, level: number, water: boolean, caught: boolean, turns: number, dark: boolean) {
	const ballLookup: {[key: string]: () => number} = {
		'Great Ball': () => 1.5,
		'Ultra Ball': () => 2,
		'Net Ball': () => {
			if (types.some((t) => ['bug', 'water'].includes(t.pokemon_v2_type.name)))
				return 3
			return 1
		},
		// Discrepancy in bulbapedia. Recheck later. Math.round may need to be removed, depending on how 3DS handled the division
		'Nest Ball': () => Math.max(Math.round(((41 - level) * 4096) / 10) / 4096, 1),
		'Dive Ball': () => {
			if (water)
				return 3.5
			return 1
		},
		'Repeat Ball': () => {
			if (caught)
				return 3.5
			return 1
		},
		'Timer Ball': () => Math.min(1 + ((turns - 1) * 1229 / 4096), 4),
		'Quick Ball': () => {
			if (turns === 1)
				return 5
			return 1
		},
		'Dusk Ball': () => {
			if (dark)
				return 3.5
			return 1
		}
	}
	const ballFunction = ballLookup[ballName] || (() => 1)
	return ballFunction()
}

export function statusLookup(chosenStatus: string) {
	if (['Sleep', 'Freeze'].includes(chosenStatus))
		return 2.5
	else if (['Poisoned', 'Burned', 'Paralyzed'].includes(chosenStatus))
		return 1.5
	return 1
}

function getGrassMod(totalCaught: number) {
	if (totalCaught > 600)
		return 1
	if (totalCaught > 450)
		return 3686 / 4096
	if (totalCaught > 300)
		return 3277 / 4096
	if (totalCaught > 150)
		return 2867 / 4096
	if (totalCaught > 30)
		return .5
	return 1229 / 4096
}

function getEntraMod(entraPower: string) {
	if (entraPower === 'No Power')
		return 100
	if (entraPower === 'Power &uarr;')
		return 110
	if (entraPower === 'Power &uarr; &uarr;' )
		return 120
	return 130
}

function getCaughtMod(totalCaught: number) {
	if (totalCaught > 600)
		return 2.5
	if (totalCaught > 450)
		return 2
	if (totalCaught > 300)
		return 1.5
	if (totalCaught > 150)
		return 1
	if (totalCaught > 30)
		return .5
	return 0
}

function roundDown(number: number): number {
	return Math.floor(number * 4096) / 4096
}

function round(number: number) {
	return Math.round(number * 4096) / 4096
}

export default generateChances