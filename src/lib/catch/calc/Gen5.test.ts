import { describe, it, expect } from 'vitest'
import generateChances, { getRate, genFiveRate, statusLookup } from './Gen5'

describe('Gen 5 catch rate calculator', () => {
	const bulbasaurInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":69,"pokemon_v2_pokemonstats":[{"base_stat":45,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":45,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}]}]}
	const bayleefInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":158,"pokemon_v2_pokemonstats":[{"base_stat":60,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":60,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"grass"}}]}]}
	const shedinjaInfo = {"capture_rate":45,"pokemon_v2_pokemons":[{"weight":12,"pokemon_v2_pokemonstats":[{"base_stat":1,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":40,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"bug"}},{"pokemon_v2_type":{"name":"ghost"}}]}]}
	const rattataInfo = {"capture_rate":255,"pokemon_v2_pokemons":[{"weight":35,"pokemon_v2_pokemonstats":[{"base_stat":30,"pokemon_v2_stat":{"name":"hp"}},{"base_stat":72,"pokemon_v2_stat":{"name":"speed"}}],"pokemon_v2_pokemontypes":[{"pokemon_v2_type":{"name":"normal"}}]}]}

	it('should calculate correct concise catch rates for all balls', () => {
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.944371675699596},{"ball":"Great Ball","breakdown":16.18943726703425},{"ball":"Ultra Ball","breakdown":20.087958649107577},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.944371675699596},{"ball":"Net Ball","breakdown":11.944371675699596},{"ball":"Nest Ball","breakdown":33.783784835598844},{"ball":"Dive Ball","breakdown":11.944371675699596},{"ball":"Repeat Ball","breakdown":11.944371675699596},{"ball":"Timer Ball","breakdown":11.944371675699596},{"ball":"Luxury Ball","breakdown":11.944371675699596},{"ball":"Heal Ball","breakdown":11.944371675699596},{"ball":"Quick Ball","breakdown":39.9384137857954},{"ball":"Dusk Ball","breakdown":11.944371675699596},{"ball":"Cherish Ball","breakdown":11.944371675699596},{"ball":"Dream Ball","breakdown":11.944371675699596}])
	})

	it('should calculate correct concise catch rates for a different pokemon', () => {
		expect(generateChances(82, bayleefInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.944371675699596},{"ball":"Great Ball","breakdown":16.18943726703425},{"ball":"Ultra Ball","breakdown":20.087958649107577},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.944371675699596},{"ball":"Net Ball","breakdown":11.944371675699596},{"ball":"Nest Ball","breakdown":33.783784835598844},{"ball":"Dive Ball","breakdown":11.944371675699596},{"ball":"Repeat Ball","breakdown":11.944371675699596},{"ball":"Timer Ball","breakdown":11.944371675699596},{"ball":"Luxury Ball","breakdown":11.944371675699596},{"ball":"Heal Ball","breakdown":11.944371675699596},{"ball":"Quick Ball","breakdown":39.9384137857954},{"ball":"Dusk Ball","breakdown":11.944371675699596},{"ball":"Cherish Ball","breakdown":11.944371675699596},{"ball":"Dream Ball","breakdown":11.944371675699596}])
	})

	it('should calculate hp differently for shedinja', () => {
		expect(generateChances(292, shedinjaInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.944371675699596},{"ball":"Great Ball","breakdown":16.18943726703425},{"ball":"Ultra Ball","breakdown":20.087958649107577},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.944371675699596},{"ball":"Net Ball","breakdown":27.22727952564797},{"ball":"Nest Ball","breakdown":33.783784835598844},{"ball":"Dive Ball","breakdown":11.944371675699596},{"ball":"Repeat Ball","breakdown":11.944371675699596},{"ball":"Timer Ball","breakdown":11.944371675699596},{"ball":"Luxury Ball","breakdown":11.944371675699596},{"ball":"Heal Ball","breakdown":11.944371675699596},{"ball":"Quick Ball","breakdown":39.9384137857954},{"ball":"Dusk Ball","breakdown":11.944371675699596},{"ball":"Cherish Ball","breakdown":11.944371675699596},{"ball":"Dream Ball","breakdown":11.944371675699596}])
		expect(generateChances(292, shedinjaInfo, 'All', 1, 'None', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.944371675699596},{"ball":"Great Ball","breakdown":16.18943726703425},{"ball":"Ultra Ball","breakdown":20.087958649107577},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.944371675699596},{"ball":"Net Ball","breakdown":27.22727952564797},{"ball":"Nest Ball","breakdown":33.783784835598844},{"ball":"Dive Ball","breakdown":11.944371675699596},{"ball":"Repeat Ball","breakdown":11.944371675699596},{"ball":"Timer Ball","breakdown":11.944371675699596},{"ball":"Luxury Ball","breakdown":11.944371675699596},{"ball":"Heal Ball","breakdown":11.944371675699596},{"ball":"Quick Ball","breakdown":39.9384137857954},{"ball":"Dusk Ball","breakdown":11.944371675699596},{"ball":"Cherish Ball","breakdown":11.944371675699596},{"ball":"Dream Ball","breakdown":11.944371675699596}])
	})

	it('should calculate rounding correctly for nest ball', () => {
		expect(generateChances(1, bulbasaurInfo, 'Nest Ball', 100, 'None', false, 1, false, 2, false, false, 'No Power', false, 0)).toStrictEqual({'breakdown': new Array(32).fill(0.3378378483559884), 'overall': 33.783784835598844})
	})

	it('should correctly calculate grassmod', () => {
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 601)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.855102495397972},{"ball":"Great Ball","breakdown":17.59247669362863},{"ball":"Ultra Ball","breakdown":21.966767322212373},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.855102495397972},{"ball":"Net Ball","breakdown":12.855102495397972},{"ball":"Nest Ball","breakdown":37.286057474227604},{"ball":"Dive Ball","breakdown":12.855102495397972},{"ball":"Repeat Ball","breakdown":12.855102495397972},{"ball":"Timer Ball","breakdown":12.855102495397972},{"ball":"Luxury Ball","breakdown":12.855102495397972},{"ball":"Heal Ball","breakdown":12.855102495397972},{"ball":"Quick Ball","breakdown":44.05271829074961},{"ball":"Dusk Ball","breakdown":12.855102495397972},{"ball":"Cherish Ball","breakdown":12.855102495397972},{"ball":"Dream Ball","breakdown":12.855102495397972}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 451)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.685049683383712},{"ball":"Great Ball","breakdown":15.963312418424957},{"ball":"Ultra Ball","breakdown":19.91291334077463},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.685049683383712},{"ball":"Net Ball","breakdown":11.685049683383712},{"ball":"Nest Ball","breakdown":33.78897206958254},{"ball":"Dive Ball","breakdown":11.685049683383712},{"ball":"Repeat Ball","breakdown":11.685049683383712},{"ball":"Timer Ball","breakdown":11.685049683383712},{"ball":"Luxury Ball","breakdown":11.685049683383712},{"ball":"Heal Ball","breakdown":11.685049683383712},{"ball":"Quick Ball","breakdown":39.96143432778152},{"ball":"Dusk Ball","breakdown":11.685049683383712},{"ball":"Cherish Ball","breakdown":11.685049683383712},{"ball":"Dream Ball","breakdown":11.685049683383712}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 301)).toStrictEqual([{"ball":"Poké Ball","breakdown":10.531607051840378},{"ball":"Great Ball","breakdown":14.36052430937643},{"ball":"Ultra Ball","breakdown":17.893082924572507},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":10.531607051840378},{"ball":"Net Ball","breakdown":10.531607051840378},{"ball":"Nest Ball","breakdown":30.326996228293996},{"ball":"Dive Ball","breakdown":10.531607051840378},{"ball":"Repeat Ball","breakdown":10.531607051840378},{"ball":"Timer Ball","breakdown":10.531607051840378},{"ball":"Luxury Ball","breakdown":10.531607051840378},{"ball":"Heal Ball","breakdown":10.531607051840378},{"ball":"Quick Ball","breakdown":35.88679481465659},{"ball":"Dusk Ball","breakdown":10.531607051840378},{"ball":"Cherish Ball","breakdown":10.531607051840378},{"ball":"Dream Ball","breakdown":10.531607051840378}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 151)).toStrictEqual([{"ball":"Poké Ball","breakdown":9.385807576794894},{"ball":"Great Ball","breakdown":12.772929871973512},{"ball":"Ultra Ball","breakdown":15.894403733762868},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":9.385807576794894},{"ball":"Net Ball","breakdown":9.385807576794894},{"ball":"Nest Ball","breakdown":26.887821874404533},{"ball":"Dive Ball","breakdown":9.385807576794894},{"ball":"Repeat Ball","breakdown":9.385807576794894},{"ball":"Timer Ball","breakdown":9.385807576794894},{"ball":"Luxury Ball","breakdown":9.385807576794894},{"ball":"Heal Ball","breakdown":9.385807576794894},{"ball":"Quick Ball","breakdown":31.82030003176182},{"ball":"Dusk Ball","breakdown":9.385807576794894},{"ball":"Cherish Ball","breakdown":9.385807576794894},{"ball":"Dream Ball","breakdown":9.385807576794894}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 31)).toStrictEqual([{"ball":"Poké Ball","breakdown":7.185931080348267},{"ball":"Great Ball","breakdown":9.758880160690941},{"ball":"Ultra Ball","breakdown":12.126517839639261},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":7.185931080348267},{"ball":"Net Ball","breakdown":7.185931080348267},{"ball":"Nest Ball","breakdown":20.46372038372856},{"ball":"Dive Ball","breakdown":7.185931080348267},{"ball":"Repeat Ball","breakdown":7.185931080348267},{"ball":"Timer Ball","breakdown":7.185931080348267},{"ball":"Luxury Ball","breakdown":7.185931080348267},{"ball":"Heal Ball","breakdown":7.185931080348267},{"ball":"Quick Ball","breakdown":24.213568334155532},{"ball":"Dusk Ball","breakdown":7.185931080348267},{"ball":"Cherish Ball","breakdown":7.185931080348267},{"ball":"Dream Ball","breakdown":7.185931080348267}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', true, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":4.842362081665567},{"ball":"Great Ball","breakdown":6.563263111921142},{"ball":"Ultra Ball","breakdown":8.143849831666174},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":4.842362081665567},{"ball":"Net Ball","breakdown":4.842362081665567},{"ball":"Nest Ball","breakdown":13.696268259625299},{"ball":"Dive Ball","breakdown":4.842362081665567},{"ball":"Repeat Ball","breakdown":4.842362081665567},{"ball":"Timer Ball","breakdown":4.842362081665567},{"ball":"Luxury Ball","breakdown":4.842362081665567},{"ball":"Heal Ball","breakdown":4.842362081665567},{"ball":"Quick Ball","breakdown":16.19141347649646},{"ball":"Dusk Ball","breakdown":4.842362081665567},{"ball":"Cherish Ball","breakdown":4.842362081665567},{"ball":"Dream Ball","breakdown":4.842362081665567}])
	})

	it('should correctly calculate caughtmod', () => {
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 601)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.855102495397972},{"ball":"Great Ball","breakdown":17.59247669362863},{"ball":"Ultra Ball","breakdown":21.966767322212373},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.855102495397972},{"ball":"Net Ball","breakdown":12.855102495397972},{"ball":"Nest Ball","breakdown":37.286057474227604},{"ball":"Dive Ball","breakdown":12.855102495397972},{"ball":"Repeat Ball","breakdown":12.855102495397972},{"ball":"Timer Ball","breakdown":12.855102495397972},{"ball":"Luxury Ball","breakdown":12.855102495397972},{"ball":"Heal Ball","breakdown":12.855102495397972},{"ball":"Quick Ball","breakdown":44.05271829074961},{"ball":"Dusk Ball","breakdown":12.855102495397972},{"ball":"Cherish Ball","breakdown":12.855102495397972},{"ball":"Dream Ball","breakdown":12.855102495397972}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 451)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.672956331458304},{"ball":"Great Ball","breakdown":17.311868808309768},{"ball":"Ultra Ball","breakdown":21.591005587591436},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.672956331458304},{"ball":"Net Ball","breakdown":12.672956331458304},{"ball":"Nest Ball","breakdown":36.58560294650181},{"ball":"Dive Ball","breakdown":12.672956331458304},{"ball":"Repeat Ball","breakdown":12.672956331458304},{"ball":"Timer Ball","breakdown":12.672956331458304},{"ball":"Luxury Ball","breakdown":12.672956331458304},{"ball":"Heal Ball","breakdown":12.672956331458304},{"ball":"Quick Ball","breakdown":43.22985738975871},{"ball":"Dusk Ball","breakdown":12.672956331458304},{"ball":"Cherish Ball","breakdown":12.672956331458304},{"ball":"Dream Ball","breakdown":12.672956331458304}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 301)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.490810167518617},{"ball":"Great Ball","breakdown":17.03126092299089},{"ball":"Ultra Ball","breakdown":21.215243852970467},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.490810167518617},{"ball":"Net Ball","breakdown":12.490810167518617},{"ball":"Nest Ball","breakdown":35.88514841877609},{"ball":"Dive Ball","breakdown":12.490810167518617},{"ball":"Repeat Ball","breakdown":12.490810167518617},{"ball":"Timer Ball","breakdown":12.490810167518617},{"ball":"Luxury Ball","breakdown":12.490810167518617},{"ball":"Heal Ball","breakdown":12.490810167518617},{"ball":"Quick Ball","breakdown":42.406996488767895},{"ball":"Dusk Ball","breakdown":12.490810167518617},{"ball":"Cherish Ball","breakdown":12.490810167518617},{"ball":"Dream Ball","breakdown":12.490810167518617}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 151)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.308664003578949},{"ball":"Great Ball","breakdown":16.750653037672006},{"ball":"Ultra Ball","breakdown":20.839482118349498},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.308664003578949},{"ball":"Net Ball","breakdown":12.308664003578949},{"ball":"Nest Ball","breakdown":35.18469389105033},{"ball":"Dive Ball","breakdown":12.308664003578949},{"ball":"Repeat Ball","breakdown":12.308664003578949},{"ball":"Timer Ball","breakdown":12.308664003578949},{"ball":"Luxury Ball","breakdown":12.308664003578949},{"ball":"Heal Ball","breakdown":12.308664003578949},{"ball":"Quick Ball","breakdown":41.58413558777707},{"ball":"Dusk Ball","breakdown":12.308664003578949},{"ball":"Cherish Ball","breakdown":12.308664003578949},{"ball":"Dream Ball","breakdown":12.308664003578949}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 31)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.126517839639261},{"ball":"Great Ball","breakdown":16.470045152353126},{"ball":"Ultra Ball","breakdown":20.46372038372856},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.126517839639261},{"ball":"Net Ball","breakdown":12.126517839639261},{"ball":"Nest Ball","breakdown":34.48423936332461},{"ball":"Dive Ball","breakdown":12.126517839639261},{"ball":"Repeat Ball","breakdown":12.126517839639261},{"ball":"Timer Ball","breakdown":12.126517839639261},{"ball":"Luxury Ball","breakdown":12.126517839639261},{"ball":"Heal Ball","breakdown":12.126517839639261},{"ball":"Quick Ball","breakdown":40.76127468678622},{"ball":"Dusk Ball","breakdown":12.126517839639261},{"ball":"Cherish Ball","breakdown":12.126517839639261},{"ball":"Dream Ball","breakdown":12.126517839639261}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":11.944371675699596},{"ball":"Great Ball","breakdown":16.18943726703425},{"ball":"Ultra Ball","breakdown":20.087958649107577},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":11.944371675699596},{"ball":"Net Ball","breakdown":11.944371675699596},{"ball":"Nest Ball","breakdown":33.783784835598844},{"ball":"Dive Ball","breakdown":11.944371675699596},{"ball":"Repeat Ball","breakdown":11.944371675699596},{"ball":"Timer Ball","breakdown":11.944371675699596},{"ball":"Luxury Ball","breakdown":11.944371675699596},{"ball":"Heal Ball","breakdown":11.944371675699596},{"ball":"Quick Ball","breakdown":39.9384137857954},{"ball":"Dusk Ball","breakdown":11.944371675699596},{"ball":"Cherish Ball","breakdown":11.944371675699596},{"ball":"Dream Ball","breakdown":11.944371675699596}])
	})

	it('should correctly calculate entrapower', () => {
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'Power &uarr;', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":12.829443628485834},{"ball":"Great Ball","breakdown":17.38906645184902},{"ball":"Ultra Ball","breakdown":21.57646631378667},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":12.829443628485834},{"ball":"Net Ball","breakdown":12.829443628485834},{"ball":"Nest Ball","breakdown":36.2871463542115},{"ball":"Dive Ball","breakdown":12.829443628485834},{"ball":"Repeat Ball","breakdown":12.829443628485834},{"ball":"Timer Ball","breakdown":12.829443628485834},{"ball":"Luxury Ball","breakdown":12.829443628485834},{"ball":"Heal Ball","breakdown":12.829443628485834},{"ball":"Quick Ball","breakdown":42.897830223957115},{"ball":"Dusk Ball","breakdown":12.829443628485834},{"ball":"Cherish Ball","breakdown":12.829443628485834},{"ball":"Dream Ball","breakdown":12.829443628485834}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'Power &uarr; &uarr;', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":13.69459658994861},{"ball":"Great Ball","breakdown":18.561697375959167},{"ball":"Ultra Ball","breakdown":23.031474361667044},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":13.69459658994861},{"ball":"Net Ball","breakdown":13.69459658994861},{"ball":"Nest Ball","breakdown":38.73416845746734},{"ball":"Dive Ball","breakdown":13.69459658994861},{"ball":"Repeat Ball","breakdown":13.69459658994861},{"ball":"Timer Ball","breakdown":13.69459658994861},{"ball":"Luxury Ball","breakdown":13.69459658994861},{"ball":"Heal Ball","breakdown":13.69459658994861},{"ball":"Quick Ball","breakdown":45.790643500456504},{"ball":"Dusk Ball","breakdown":13.69459658994861},{"ball":"Cherish Ball","breakdown":13.69459658994861},{"ball":"Dream Ball","breakdown":13.69459658994861}])
		expect(generateChances(1, bulbasaurInfo, 'All', 100, 'None', false, 1, false, 1, false, false, 'Power &uarr; &uarr; &uarr;', false, 0)).toStrictEqual([{"ball":"Poké Ball","breakdown":14.541888929626948},{"ball":"Great Ball","breakdown":19.710119959624105},{"ball":"Ultra Ball","breakdown":24.456444543881965},{"ball":"Master Ball","breakdown":100},{"ball":"Premier Ball","breakdown":14.541888929626948},{"ball":"Net Ball","breakdown":14.541888929626948},{"ball":"Nest Ball","breakdown":41.13067309360319},{"ball":"Dive Ball","breakdown":14.541888929626948},{"ball":"Repeat Ball","breakdown":14.541888929626948},{"ball":"Timer Ball","breakdown":14.541888929626948},{"ball":"Luxury Ball","breakdown":14.541888929626948},{"ball":"Heal Ball","breakdown":14.541888929626948},{"ball":"Quick Ball","breakdown":48.62373618865994},{"ball":"Dusk Ball","breakdown":14.541888929626948},{"ball":"Cherish Ball","breakdown":14.541888929626948},{"ball":"Dream Ball","breakdown":14.541888929626948}])
	})

	it('should handle x going above 255', () => {
		expect(generateChances(19, rattataInfo, 'Poké Ball', 1, 'Freeze', false, 1, false, 1, false, false, 'No Power', false, 0)).toStrictEqual({ 'breakdown': new Array(32).fill(1), 'overall': 100 })
	})

})

describe('Gen 5 status rates', () => {
	it('should return 2.5 if asleep or frozen', () => {
		expect(statusLookup('Sleep')).toBe(2.5)
		expect(statusLookup('Freeze')).toBe(2.5)
	})

	it('should return 1.5 if poisoned, burned, or paralyzed', () => {
		expect(statusLookup('Poisoned')).toBe(1.5)
		expect(statusLookup('Burned')).toBe(1.5)
		expect(statusLookup('Paralyzed')).toBe(1.5)
	})

	it('should return 1 otherwise', () => {
		expect(statusLookup('None')).toBe(1)
	})
})

describe('Gen 5 ball rates', () => {
	describe('Great Ball', () => {
		const ball = 'Great Ball'
		it('should return 1.5', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1.5)
		})
	})
	describe('Ultra Ball', () => {
		const ball = 'Ultra Ball'
		it('should return 2', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(2)
		})
	})
	describe('Net Ball', () => {
		const ball = 'Net Ball'
		it('should return 3 if bug or water', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"bug"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(3)
			expect(genFiveRate([{"pokemon_v2_type":{"name":"water"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(3)
		})
		it('should return 1 otherwise', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Nest Ball', () => {
		const ball = 'Nest Ball'
		it('should return 4 at level 1', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(4)
		})
		it('should return 3.9 at level 2', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 2, false, false, 1, false)).toBe(15974 / 4096)
		})
		it('should return 3 at level 11', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 11, false, false, 1, false)).toBe(3)
		})
		it('should return 2 at level 21', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 21, false, false, 1, false)).toBe(2)
		})
		it('should return 1 at level 31', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 31, false, false, 1, false)).toBe(1)
		})
		it('should return 1 at any level higher', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 50, false, false, 1, false)).toBe(1)
		})
	})
	describe('Dive Ball', () => {
		const ball = 'Dive Ball'
		it('should return 3.5 if fishing', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, true, false, 1, false)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Repeat Ball', () => {
		const ball = 'Repeat Ball'
		it('should return 3.5 if caught before', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, true, 1, false)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
	describe('Timer Ball', () => {
		const ball = 'Timer Ball'
		it('should return 1 at turn 1', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
		it('should return 1.60009765625 at turn 2', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 2, false)).toBe(1.300048828125)
		})
		it('should return 3 at turn 9', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 9, false)).toBe(3.400390625)
		})
		it('should return 3 at turn 10', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 10, false)).toBe(3.700439453125)
		})
		it('should return 4 at turn 11', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 11, false)).toBe(4)
		})
		it('should return 4 at any higher', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 12, false)).toBe(4)
		})
	})
	describe('Quick Ball', () => {
		const ball = 'Quick Ball'
		it('should return 5 on turn 1', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(5)
		})
		it('should return 1 otherwise', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 4, false)).toBe(1)
		})
	})
	describe('Dusk Ball', () => {
		const ball = 'Dusk Ball'
		it('should return 3.5 if dark', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, true)).toBe(3.5)
		})
		it('should return 1 otherwise', () => {
			expect(genFiveRate([{"pokemon_v2_type":{"name":"grass"}},{"pokemon_v2_type":{"name":"poison"}}], ball, 1, false, false, 1, false)).toBe(1)
		})
	})
})