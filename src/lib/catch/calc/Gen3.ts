const changesSpeed: Record<number, number> = {
	18: 91,
	26: 100,
	85: 100,
	101: 140,
	284: 60,
	301: 70
}
const changesHp: Record<number, number> = {
	219: 50,
	222: 55,
	226: 65,
	337: 70,
	338: 70,
	358: 65
}
const changesRate: Record<number, number> = {
	382: 5,
	383: 5,
	384: 3,
	483: 30,
	484: 30
}
const colloseumRate: Record<number, number> = {
	153: 180,
	156: 180,
	159: 180,
	176: 45,
	200: 90,
	226: 90,
	227: 15,
	243: 15,
	244: 15,
	245: 15,
	248: 10,
	296: -1,
	357: 45,
	376: 15
}
const xdRate: Record<number, number> = {
	12: 90,
	15: 90,
	49: 120,
	51: 100,
	55: 120,
	57: 80,
	62: 90,
	78: 110,
	82: 110,
	83: 80,
	85: 90,
	97: 80,
	103: 80,
	105: 110,
	106: 90,
	107: 90,
	108: 90,
	112: 100,
	113: 70,
	114: 90,
	115: 90,
	121: 110,
	122: 90,
	123: 90,
	125: 90,
	126: 90,
	127: 90,
	128: 80,
	131: 80,
	143: 70,
	144: 25,
	145: 25,
	146: 25,
	216: -1,
	219: 120,
	228: 225,
	277: 90,
	301: 120,
	302: 90,
	303: 120,
	310: 80,
	334: 80,
	337: 100,
	338: 90,
	354: 90,
	373: 80
}
export const ballList: string[] = [
	'Poké Ball',
	'Great Ball',
	'Ultra Ball',
	'Safari Ball',
	'Master Ball',
	'Premier Ball',
	'Net Ball',
	'Nest Ball',
	'Dive Ball',
	'Repeat Ball',
	'Timer Ball',
	'Luxury Ball',
	'Heal Ball',
	'Quick Ball',
	'Dusk Ball',
	'Park Ball',
	'Cherish Ball',
	'Sport Ball',
	'Friend Ball',
	'Fast Ball',
	'Heavy Ball',
	'Level Ball',
	'Love Ball',
	'Lure Ball',
	'Moon Ball',
	'All'
]
export const statusList: string[] = ['None', 'Sleep', 'Freeze', 'Poisoned', 'Burned', 'Paralyzed']
export const end = 493

function generateChances(dex: number, info: catchInfo, shadow: string, ball: string, hp: number, level: number, chosenStatus: string, yourLevel: number, playerPokemon: boolean, wildGender: string, yourGender: string, fishing: boolean, caught: boolean, turns: number, dark: boolean) {
	let catchRate: number
	if (shadow === 'Yes (Colosseum)')
		catchRate = colloseumRate[dex] ?? info.capture_rate
	else if (shadow === 'Yes (XD)')
		catchRate = xdRate[dex] ?? info.capture_rate
	else
		catchRate = changesRate[dex] ?? info.capture_rate
	const weight = info.pokemon_v2_pokemons[0].weight
	interface pokemonStat {
		base_stat: number,
		pokemon_v2_stat: {
			name: string
		}
	}
	const speed: number = changesSpeed[dex] ?? info.pokemon_v2_pokemons[0].pokemon_v2_pokemonstats.find((stat: pokemonStat) => stat.pokemon_v2_stat.name === 'speed')?.base_stat
	const hpMax: number = changesHp[dex] ?? info.pokemon_v2_pokemons[0].pokemon_v2_pokemonstats.find((stat: pokemonStat) => stat.pokemon_v2_stat.name === 'hp')?.base_stat
	const types: {pokemon_v2_type: {name: string}}[] = info.pokemon_v2_pokemons[0].pokemon_v2_pokemontypes
	if (ball !== 'All') {
		const chances = getChances(dex, catchRate, hp, hpMax, weight, speed, types, ball, level, chosenStatus, yourLevel, playerPokemon, wildGender, yourGender, fishing, caught, turns, dark)
		return { overall: chances.reduce((a, b) => {return a + b}, 0) / chances.length * 100, breakdown: chances }
	}
	const overall: {ball: string, breakdown: number}[] = []
	ballList.forEach((ball) => {
		if (ball === 'All')
			return
		const chances = getChances(dex, catchRate, hp, hpMax, weight, speed, types, ball, level, chosenStatus, yourLevel, playerPokemon, wildGender, yourGender, fishing, caught, turns, dark)
		const totalChances = chances.reduce((a, b) => {return a + b}, 0) / chances.length * 100
		overall.push({ball, breakdown: totalChances})
	})
	return overall
}
function getChances(dex: number, rate: number, hp: number, hpMax: number, weight: number, speed: number, types: {pokemon_v2_type: {name: string}}[], ballName: string, level: number, chosenStatus: string, yourLevel: number, playerPokemon: boolean, wildGender: string, yourGender: string, fishing: boolean, caught: boolean, turns: number, dark: boolean) {
	if (['Master Ball', 'Park Ball'].includes(ballName) || rate === -1)
		return new Array(32).fill(1)
	return Array.from({length: 32}, (_, i) => {
		let hpmax = hpMax === 1 ? 1 : Math.floor((((2 * hpMax + i) * level) / 100) + level + 10)
		const hpcurr = Math.max(Math.round(hpmax * hp / 100), 1) << 1
		hpmax += hpmax << 1
		const num = Math.max(1, Math.floor(Math.floor((hpmax - hpcurr) * Math.floor((getApricorn(dex, weight, rate, speed, ballName, yourLevel, level, playerPokemon, wildGender, yourGender, fishing)) * (getRate(types, ballName, level, fishing, caught, turns, dark))) / hpmax) * statusLookup(chosenStatus)))
		return num >= 255 ? 1 : Math.pow(Math.min((Math.floor(1048560 / Math.floor(Math.sqrt(Math.floor(Math.sqrt(16711680 / num)))))) / 65536, 1), 4)
	})
}
export function getRate(types: {pokemon_v2_type: {name: string}}[], ballName: string, level: number, fishing: boolean, caught: boolean, turns: number, dark: boolean) {
	const ballLookup: {[key: string]: () => number} = {
		'Great Ball': () => 1.5,
		'Safari Ball': () => 1.5,
		'Sport Ball': () => 1.5,
		'Ultra Ball': () => 2,
		'Net Ball': () => {
			if (types.some((t) => ['bug', 'water'].includes(t.pokemon_v2_type.name)))
				return 3
			return 1
		 },
		'Nest Ball': () => Math.max((40 - level) / 10, 1),
		'Dive Ball': () => {
			if (fishing)
				return 3.5
			return 1
		},
		'Repeat Ball': () => {
			if (caught)
				return 3.5
			return 1
		},
		'Timer Ball': () => Math.min((turns + 9) / 10, 4),
		'Quick Ball': () => {
			if (turns === 1)
				return 4
			return 1

		},
		'Dusk Ball': () => {
			if (dark)
				return 3.5
			return 1
		}
	}
	const ballFunction = ballLookup[ballName] || (() => 1)
	return ballFunction()
}
export function getApricorn(dex: number, weight: number, rate: number, speed: number, ballName: string, yourLevel: number, level: number, playerPokemon: boolean, wildGender: string, yourGender: string, fishing: boolean) {
	const ballLookup: {[key: string]: () => number} = {
		'Fast Ball': () => {
			if (speed >= 100)
				return rate * 4
			return rate
		},
		'Heavy Ball': () => {
			if (weight >= 4096)
				return rate + 40
			if (weight >= 3072)
				return rate + 30
			if (weight >= 2048)
				return rate + 20
			return rate - 20
		},
		'Level Ball': () => {
			if (yourLevel >> 2 > level)
				return rate * 8
			if (yourLevel >> 1 > level)
				return rate * 4
			if (yourLevel > level)
				return rate * 2
			return rate
		},
		'Love Ball': () => {
			if (playerPokemon && wildGender !== yourGender && ![wildGender, yourGender].includes('Genderless'))
				return rate * 8
			return rate
		},
		'Lure Ball': () => {
			if (fishing)
				return rate * 3
			return rate
		},
		'Moon Ball': () => {
			if ([29, 30, 31, 32, 33, 34, 35, 36, 39, 40, 300, 301].includes(dex))
				return rate * 4
			return rate
		}
	}
	const ballFunction = ballLookup[ballName] || (() => rate)
	return Math.max(Math.min(ballFunction(), 255), 1)
}
export function statusLookup(chosenStatus: string) {
	if (['Sleep', 'Freeze'].includes(chosenStatus))
		return 2
	else if (['Poisoned', 'Burned', 'Paralyzed'].includes(chosenStatus))
		return 1.5
	return 1
}
export default generateChances