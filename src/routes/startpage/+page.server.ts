import image from '$lib/thumbnails/startpage.png'
import type { PageServerLoad } from './$types'

export const load: PageServerLoad = async ({ fetch, platform, cookies, setHeaders }) => {
	const differentLoc = cookies.get('lat')
	const lat = differentLoc ?? platform?.cf.latitude ?? '39.878056'
	const lon = cookies.get('lon') ?? platform?.cf.longitude ?? '-83.078056'
	const city = differentLoc ? '' : (platform?.cf.city ?? '')
	const state = differentLoc ? (platform?.cf.country ?? 'US') : (platform?.cf.region ?? 'Ohio')
	const cookie = cookies.get('units')
	const units = cookie ?? (['US', 'KY', 'LR'].includes(platform?.cf.country ?? 'US') ? 'imperial' : 'metric')
	if (cookie)
		await fetch('api/units', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ units })
		})
	if (differentLoc)
		await fetch('api/zip', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				lat: differentLoc,
				lon: cookies.get('lon')
			})
		})
	const [weather, calendar] = await Promise.all([
		fetch(`api/weather?${new URLSearchParams({ lat, lon, city, state, units }).toString()}`),
		fetch('api/calendar')
	])

	setHeaders({
		'Server-Timing': [weather.headers.get('server-timing'), calendar.headers.get('server-timing')].join(',')
	})

	return {
		title: 'Startpage | Weather, Calendar, & Search Functionality',
		description: 'A personalized startpage with weather, calendar, and search functionality that can help you stay organized and on top of your schedule.',
		image,
		country: platform?.cf.country ?? 'US',
		timezone: platform?.cf.timezone ?? 'America/New_York',
		weather: await weather.json() as weatherData,
		calendar: await calendar.json() as {title: string, dueDate: string, url: string}[]
	}
}