import image from '$lib/thumbnails/index.png'
export const prerender = true
import type { PageServerLoad } from './$types'

export const load: PageServerLoad = async () => {
    return {
        title: 'Josh Stock | Web Developer & Cyber Security Student',
        description: "Explore the portfolio of web developer and computer science and cyber security student Joshua Stock. Check out his projects and see what Josh's been up to!",
        image
    }
}