export const prerender: boolean = true

const addDays = function(date: Date, days: number): Date {
	const newDate = new Date(date)
	newDate.setDate(newDate.getDate() + days)
	return newDate
}

export const GET = async (): Promise<Response> => {
	const nextYear: Date = addDays(new Date(), 365)
	
	const text: string = `
	Contact: https://joshuastock.net
	Contact: mailto:security@joshuastock.net
	Expires: ${nextYear.toISOString()}
	Preferred-Languages: en
	Canonical: https://joshuastock.net/.well-know/security.txt
	`
	
	const headers: HeadersInit = {
		'Content-Type': 'text/plain; charset=utf-8',
	}
	
	return new Response(text, { headers })
}