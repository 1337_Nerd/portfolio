export const prerender: boolean = true
export const GET = async (): Promise<Response> => {
	const text: string = `
	[{
		"user_agent": "prefetch-proxy",
		"google_prefetch_proxy_eap": {
			"fraction": 1.0
		}
	}]`

	const headers: HeadersInit = {
		'Content-Type': 'application/trafficadvice+json',
	}
	
	return new Response(text, { headers })
}