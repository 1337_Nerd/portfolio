import { type RequestHandler, json } from '@sveltejs/kit'

export const GET: RequestHandler = async({ url, setHeaders }) => {
	const dex: number = +(url.searchParams.get('dex') || 1)
	const query = `query pokemonInfo {
		pokemon_v2_pokemonspecies(where: {id: {_eq: ${dex}}}) {
			capture_rate
			pokemon_v2_pokemons(where: {is_default: {_eq: true}}) {
				weight
				pokemon_v2_pokemonstats(where: {pokemon_v2_stat: {name: {_iregex: "hp|speed"}}}) {
					base_stat
					pokemon_v2_stat {
						name
					}
				}
				pokemon_v2_pokemontypes {
					pokemon_v2_type {
						name
					}
				}
			}
		}
	}`
	const res = await fetch('https://beta.pokeapi.co/graphql/v1beta',
		{
			method: 'POST',
			body: JSON.stringify({
				query: query,
			})
		})
	const expires = res.headers.get('expires')
	const cacheControl = res.headers.get('cache-control')
	if (expires)
		setHeaders({'expires': expires})
	if (cacheControl)
		setHeaders({'cache-control': cacheControl})
	const { data }: {data: pokemonInfo} = await res.json()
	return json(data.pokemon_v2_pokemonspecies[0])
}