import { type RequestHandler, json } from '@sveltejs/kit'
export const POST: RequestHandler = async({ request, cookies }) => {
	const { units } = await request.json()
	cookies.set('units', units, {
		path: '/startpage',
		maxAge: 60 * 60 * 24,
		sameSite: 'strict'
	})
	return json({units})
}