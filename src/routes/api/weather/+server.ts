import { type RequestHandler, json } from '@sveltejs/kit'
import clear from '$lib/assets/icons/clear-day.svg'
import partlyCloudy from '$lib/assets/icons/partly-cloudy-day.svg'
import cloudy from '$lib/assets/icons/cloudy.svg'
import drizzle from '$lib/assets/icons/drizzle.svg'
import partlyCloudyRain from '$lib/assets/icons/partly-cloudy-day-rain.svg'
import thunderstorms from '$lib/assets/icons/thunderstorms.svg'
import snow from '$lib/assets/icons/snow.svg'
import mist from '$lib/assets/icons/mist.svg'
import dust from '$lib/assets/icons/dust-wind.svg'
import fog from '$lib/assets/icons/fog.svg'
import haze from '$lib/assets/icons/haze.svg'
import smoke from '$lib/assets/icons/smoke.svg'
import tornado from '$lib/assets/icons/tornado.svg'
import overcast from '$lib/assets/icons/overcast.svg'
const cacheSeconds = 600

const lookup: {[key: string]: {icon: string, alt: string}} = {
	'01': {
		icon: clear,
		alt: 'Clear'
	},
	'02': {
		icon: partlyCloudy,
		alt: 'Partly Cloudy'
	},
	'03': {
		icon: cloudy,
		alt: 'Cloudy'
	},
	'09': {
		icon: drizzle,
		alt: 'Drizzle'
	},
	'10': {
		icon: partlyCloudyRain,
		alt: 'Partly cloudy rain'
	},
	'11': {
		icon: thunderstorms,
		alt: 'Thunderstorms'
	},
	'13': {
		icon: snow,
		alt: 'Snow'
	},
	'50': {
		icon: mist,
		alt: 'Mist'
	},
	'Dust': {
		icon: dust,
		alt: 'Dust'
	},
	'Fog': {
		icon: fog,
		alt: 'Fog'
	},
	'Haze': {
		icon: haze,
		alt: 'Haze'
	},
	'Smoke': {
		icon: smoke,
		alt: 'Smoke'
	},
	'Tornado': {
		icon: tornado,
		alt: 'Tornado'
	}
}

export const GET: RequestHandler = async({ url, platform, setHeaders, fetch }) => {
	const apikey = platform?.env.apikey
	const lat: string = url.searchParams.get('lat') as string
	const lon: string = url.searchParams.get('lon') as string
	const units: string = url.searchParams.get('units') as string
	let locationStart = NaN, locationEnd = NaN, start = NaN, end = NaN
	const fetchLocation = async() => {
		const cur = `https://api.openweathermap.org/data/3.0/find?lat=${lat}&lon=${lon}&appid=${apikey}&units=${units}`
		locationStart = performance.now()
		const res = await fetch(cur)
		locationEnd = performance.now()
		const data: openweathermapFindData = await res.json()
		const closest = data.list?.[0] ?? { name: 'Grove City', id: '4513409', state: 'US' }
		return { city: closest.name, id: closest.id, state: closest.sys?.country }
	}
	const fetchCurrent = async() => {
		const cur = `https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely&appid=${apikey}&units=${units}`
		start = performance.now()
		const res = await fetch(cur)
		end = performance.now()
		const data: openweathermapData = await res.json()
		const temp: number = Math.round(data.current?.temp ?? 0)
		const condition: string = data.current?.weather?.[0]?.description ?? 'not available'
		const closest = await fetchLocation()
		const result = {
			units,
			lat,
			lon,
			url: `https://openweathermap.org/city/${closest.id}`,
			alerts: data.alerts?.map(alert => ({
				event: alert.event,
				start: alert.start * 1000,
				end: alert.end * 1000
			})),
			temp: temp,
			feels_like: data.daily?.[0]?.feels_like ?? {'day': 99, 'night': 99, 'eve': 99, 'morn': 99},
			condition: condition,
			days: (data.daily ?? [{},{},{}]).slice(0, 3).map(day => ({
				max: Math.floor(day?.temp?.max ?? 0),
				min: Math.floor(day?.temp?.min ?? 0),
				precip: isNaN(day?.pop) ? 'N/A%' : `${Math.round(day?.pop * 100)}%`,
				humidity: `Humidity: ${day?.humidity ?? 'N/A'}%`,
				wind: `Wind: ${day?.wind_speed ?? 0}mph`,
				raining: `${day?.snow || day?.weather?.[0]?.main === 'Snow' || temp <= 32 ? 'Snow' : 'Rain'}: ${parseFloat(((day?.snow ?? day?.rain ?? 0) / 25.4).toFixed(3))}"`,
				pres: `Pressure: ${day?.pressure ?? 0}hPa`,
				icons: lookup[/smoke|haze|dust|fog|tornado/i.test(condition) ? condition : day?.weather?.[0]?.icon?.slice(0, -1)] ?? { icon: overcast, alt: 'Overcast' }
			})),
			location: {city: url.searchParams.get('city') || closest.city, state: url.searchParams.get('state') || closest.state}
		}
		return result
	}
	const current = await fetchCurrent()
	const currentTimings = formatString('current weather', end - start)
	const locationTimings = formatString('location lookup', locationEnd - locationStart)
	setHeaders({
		'Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Cloudflare-CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Server-Timing': [currentTimings, locationTimings].join(',')
	})
	return json(current)
}

function formatString(name: string, duration: number) {
	return `${name};dur=${duration};desc="${name}"`
}