import { type RequestHandler, json } from '@sveltejs/kit'

const cacheSeconds = 600
const base = 'https://onu.instructure.com'

export const GET: RequestHandler = async({ platform, fetch, setHeaders }) => {
	let start = NaN, end = NaN, processTime: string | undefined
	const accessToken = platform?.env.accessToken
	const fetchCalendar = async() => {
		const cur = `${base}/api/v1/planner/items?start_date=${new Date().toISOString()}&per_page=100&order=asc`
		start = performance.now()
		const res = await fetch(cur, {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		})
		end = performance.now()
		processTime = res.headers.get('x-runtime') ?? undefined
		const data: {errors?: {message: string}[]} = await res.json()
		if (data.errors)
			return []
		const responseData: plannerData[] = data as plannerData[]
		return responseData.filter(assignment => assignment.plannable_type !== 'announcement' && ![9620, 8777].includes(assignment.course_id) && !(assignment.submissions.submitted || assignment.submissions.graded) && !assignment.submissions.excused).map(assignment => ({ title: assignment.plannable.title, dueDate: assignment.plannable.due_at, url: `${base}${assignment.html_url}` }))
	}
	const cal = await fetchCalendar()
	setHeaders({
		'Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Cloudflare-CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Server-Timing': `canvas;dur=${+(processTime ?? '0') * 1000};desc="canvas processing time",calendar;dur=${end - start};desc="canvas calendar lookup"`
	})
	return json(cal)
}