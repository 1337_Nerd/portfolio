import { json } from '@sveltejs/kit'
import type { ActionResult, RequestHandler } from '@sveltejs/kit'
export const POST: RequestHandler = async({ platform, request }) => {
	const webhookUrl = platform?.env.webhookUrl ?? ''
	try {
		const formData = await request.formData()
		if (formData.get('alternateEmail'))
			return json({ type: 'error', status: 400, error: 'Spam detected' })
		const { subject, name, email, message } = Object.fromEntries(formData.entries())
		const webhookBody = {
			embeds: [{
				title: `New contact form submission from ${email as string}`,
				fields: [
					{ name: 'Sender', value: name },
					{ name: 'Subject', value: subject},
					{ name: 'Message', value: message}
				]
			}]
		}
		const res = await fetch(webhookUrl, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(webhookBody)
		})
		if (!res.ok)
			throw new Error(res.statusText, { cause: res.status })
		const successResponse: ActionResult = {
			type: 'success',
			status: res.status
		}
		return json(successResponse)
	}
	catch (e: any) {
		const failureResponse: ActionResult = {
			type: 'error',
			status: e.cause,
			error: e.message
		}
		return json(failureResponse)
	}
}
