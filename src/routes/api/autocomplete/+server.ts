import { type RequestHandler, json } from '@sveltejs/kit'
const cacheSeconds = 3600
export const GET: RequestHandler = async({ url, setHeaders }) => {
	const query = url.searchParams.get('q')
	const start = performance.now()
	const res = await fetch(`https://duckduckgo.com/ac/?q=${query}&kl=wt-wt`)
	const data = await res.json()
	setHeaders({
		'Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Cloudflare-CDN-Cache-Control': `public, max-age=${cacheSeconds}, s-maxage=${cacheSeconds}`,
		'Server-Timing': `duckduckgo;dur=${performance.now() - start};desc="duckduckgo autocomplete"`
	})
	return json(data)
}