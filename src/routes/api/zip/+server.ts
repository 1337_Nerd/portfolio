import { type RequestHandler, json } from '@sveltejs/kit'
export const POST: RequestHandler = async({ request, cookies }) => {
	const { lat, lon } = await request.json()
	cookies.set('lat', lat, {
		path: '/startpage',
		maxAge: 60 * 60 * 24,
		sameSite: 'strict'
	})
	cookies.set('lon', lon, {
		path: '/startpage',
		maxAge: 60 * 60 * 24,
		sameSite: 'strict'
	})
	return json({lat, lon})
}
