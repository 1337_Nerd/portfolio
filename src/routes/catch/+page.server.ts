import image from '$lib/thumbnails/catch.png'
import type { Load } from '@sveltejs/kit'
const pokemonList = `query pokemonList {
	pokemon_v2_pokemon(order_by: {id: asc}, where: {is_default: {_eq: true}}) {
		name
		pokemon_species_id
	}
}`

export const prerender = true

export const load: Load = async ({fetch}) => {
	const fetchData = async() => {
		let data: pokemonList
		try {
			const res = await fetch('https://beta.pokeapi.co/graphql/v1beta',
			{
				method: 'POST',
				body: JSON.stringify({
					query: pokemonList,
				})
			})
			data = (await res.json()).data
		}
		catch {
			data = {pokemon_v2_pokemon: [{name: 'Bulbasaur', pokemon_species_id: 1}]}
		}
		return data
	}
	const { pokemon_v2_pokemon } = await fetchData()
	return {
		pokemon_v2_pokemon,
		title: 'Pokémon Catch Rate Calculator for Gen 2, 3, & 4 Games',
		description: 'This Pokémon catch rate calculator is designed for Gen 2, 3, 4, and 5 Pokémon games. Determine the chance of capturing Pokémon with ease by using this calculator.',
		image
	}
}