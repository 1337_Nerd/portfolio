const url = 'https://joshuastock.net/'
import { exec } from 'child_process'
import { promisify } from 'util'
import resume from '$lib/assets/JoshStockResume.pdf?url'

const getGitLog = async (location: string | null) => {
	const execPromise = promisify(exec)
	let stdout = ''
	try {
		stdout = (await execPromise(`git log -1 --pretty=format:%cI ${location}`)).stdout
	}
	catch {
		stdout = (await execPromise(`git log -1 --pretty=format:%cI src/lib/assets/JoshStockResume.pdf`)).stdout
	}
	return stdout.trim()
}

const staticPages = Object.keys(import.meta.glob('/src/routes/**/+page.(svelte|md)'))
.concat(resume)

const pages = await Promise.all(staticPages.map(async (page) => {
	const matchResult = /\/(.*)\/.*$/i.exec(page)
	const location = matchResult ? matchResult[1] : null
	const gitLog = await getGitLog(location)
	console.log(page, gitLog)
	page = page
	.replace('/src/routes', '')
	.replace('/+page.svelte', '')
	.replace('/static/', '')
	page = new URL(page, url).toString()
	return {
		page,
		time: gitLog
	}
}))

export const prerender = true

export const GET = async (): Promise<Response> => {
	const headers: Record<string, string> = {
		'Cache-Control': 'max-age=3600',
		'Content-Type': 'application/xml'
	}
	const urlset = pages
	.map((page: {page: string, time: string}) => `
	<url>
		<loc>${page.page}</loc>
		<lastmod>${page.time}</lastmod>
	</url>`)
	.join('')
	
	const xml = `<?xml version="1.0" encoding="UTF-8" ?>
	<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
	xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">${urlset}
</urlset>`
	return new Response(xml, { headers })
}