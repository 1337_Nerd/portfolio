import type { LayoutServerLoad } from './$types'
export const load: LayoutServerLoad = async ({ setHeaders }) => {
	try {
		setHeaders({
			'Cache-Control': 'no-cache',
			'Referrer-Policy': 'no-referrer',
			'Cross-Origin-Resource-Policy': 'same-site',
			'X-Content-Type-Options': 'nosniff',
			'Permissions-Policy': 'accelerometer=(), autoplay=(), camera=(), cross-origin-isolated=(self), display-capture=(), encrypted-media=*, fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), payment=(), picture-in-picture=(self), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(self), web-share=(), xr-spatial-tracking=(), clipboard-read=(self), clipboard-write=(self), gamepad=()'
		})
	}
	catch { /* Empty */ }
}