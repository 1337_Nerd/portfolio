import image from '$lib/thumbnails/pokedex.png'
import type { Load } from '@sveltejs/kit'
const pokemonList = `query pokemonList {
	pokemon_v2_pokemon(order_by: {id: asc}, where: {is_default: {_eq: true}}) {
		name
		pokemon_species_id
	}
}`

export const prerender = true

export const load: Load = async ({fetch}) => {
	const fetchData = async() => {
		const res = await fetch('https://beta.pokeapi.co/graphql/v1beta',
		{
			method: 'POST',
			body: JSON.stringify({
				query: pokemonList,
			})
		})
		try {
			return await res.json()
		}
		catch {
			return {data: {pokemon_v2_pokemon: [{name: 'Bulbasaur', pokemon_species_id: 1}]}}
		}
	}
	const { data } = await fetchData()
	return {
		data,
		title: 'Comprehensive Pokédex | Sorted by National Dex',
		description: 'A Pokédex! Sorted by National Dex, making it easy to keep track of all Pokémon from Johto and beyond.',
		image
	}
}