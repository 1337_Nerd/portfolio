import { redirect } from '@sveltejs/kit'
import resume from '$lib/assets/resumes/JoshStockResumeCyber.pdf?url'

export const GET = async() => redirect(302, resume)