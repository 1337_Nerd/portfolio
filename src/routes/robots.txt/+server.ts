export const prerender = false

export async function GET({ platform }) {
	const { ACCESS_TOKEN } = platform?.env ?? import.meta.env
	const res = await fetch('https://api.darkvisitors.com/robots-txts', {
		method: 'POST',
		headers: {
			'Authorization': `Bearer ${ACCESS_TOKEN}`,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			'agent_types': [
				'AI Assistant',
				'AI Data Scraper',
				'AI Search Crawler',
				'Undocumented AI Agent'
			],
			'disallow': '/'
		})
	})
	const data = `${await res.text()}\n\nSitemap: https://joshuastock.net/sitemap.xml`
	return new Response(data, {
		headers: {
			'Content-Type': 'text/plain'
		}
	})
}