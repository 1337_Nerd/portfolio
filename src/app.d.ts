// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
type TrackedProperties = {
	/**
	 * Hostname of server
	 * @description extracted from `window.location.hostname`
	 * @example 'analytics.umami.is'
	 */
	hostname: string;
	
	/**
	 * Browser language
	 *
	 * @description extracted from `window.navigator.language`
	 * @example 'en-US', 'fr-FR'
	 */
	language: string;
	
	/**
	 * Page referrer
	 *
	 * @description extracted from `window.navigator.language`
	 * @example 'https://analytics.umami.is/docs/getting-started'
	 */
	referrer: string;
	
	/**
	 * Screen dimensions
	 *
	 * @description extracted from `window.screen.width` and `window.screen.height`
	 * @example '1920x1080', '2560x1440'
	 */
	screen: string;
	
	/**
	 * Page title
	 *
	 * @description extracted from `document.querySelector('head > title')`
	 * @example 'umami'
	 */
	title: string;
	
	/**
	 * Page url
	 *
	 * @description built from `${window.location.pathname}${window.location.search}`
	 * @example 'docs/getting-started'
	 */
	url: string;
	
	/**
	 * Website ID (required)
	 *
	 * @example 'b59e9c65-ae32-47f1-8400-119fcf4861c4'
	 */
	website: string;
}
type EventProperties = { name: string; data?: EventData; } & TrackedProperties & { website: string }
type PageViewProperties = TrackedProperties & { website: string }
type CustomEventFunction = (props: PageViewProperties) => EventProperties | PageViewProperties
interface EventData { [key: string]: number | string | EventData | number[] | string[] | EventData[] }
declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		interface Platform {
			env: {
				apikey: string,
				accessToken: string,
				account: string,
				authUrl: string,
				ACCESS_TOKEN: string,
				webhookUrl: string
			}
			context: {
				waitUntil(promise: Promise<unknown>): void
			}
			caches: CacheStorage & {default: Cache}
			cf: {
				asn: number,
				asOrganization: string,
				botManagement: {
					score: number,
					captcha: boolean,
					allowed: boolean
				} | null,
				clientAcceptEncoding: string | null,
				colo: string,
				country: string | null,
				isEUCountry: string | null,
				httpProtocol: string,
				requestPriority: string | null,
				tlsCipher: string,
				tlsClientAuth: {
					certFingerprintSHA1: string,
					certFingerprintSHA256: string,
					certIssuerDN: string,
					certIssuerDNLegacy: string,
					certIssuerDNRFC2253: string,
					certIssuerSKI: string,
					certIssuerSerial: string,
					certNotAfter: string,
					certNotBefore: string,
					certPresented: string,
					certRevoked: string,
					certSKI: string,
					certSerial: string,
					certSubjectDN: string,
					certSubjectDNLegacy: string,
					certSubjectDNRFC2253: string,
					certVerifie: stringd
				} | null,
				tlsVersion: string,
				city: string | null,
				continent: string | null,
				latitude: string | null,
				longitude: string | null,
				postalCode: string | null,
				metroCode: string | null,
				region: string | null,
				regionCode: string | null,
				timezone: string
			}
		}
	}
	interface Window {
		trustedTypes?: {
			createPolicy(policyName: string, policyConfig: TrustedTypesPolicyOptions): TrustedTypePolicy
		},
		umami?: {
			track: {
				(): Promise<string>,
				(eventName: string): Promise<string>,
				(eventName: string, obj: EventData): Promise<string>,
				(properties: PageViewProperties): Promise<string>,
				(eventFunction: CustomEventFunction): Promise<string>
			}
		}
	}
	interface weatherData  {
		units: string,
		lat: string,
		lon: string,
	    url: string,
	    alerts?: {
	        event: string,
	        start: number,
	        end: number
	    }[],
	    temp: number,
		feels_like: {
			morn: number,
			day: number,
			eve: number,
			night: number
		},
	    condition: string,
	    days: {
	        max: number,
	        min: number,
	        precip: string,
	        humidity: string,
	        wind: string,
	        raining: string,
	        pres: string,
	        icons: {
				icon: string,
				alt: string
			}
	    }[],
	    location: {
	    	city: string,
	    	state: string
	    }
	}
	interface openweathermapFindData {
		message: string,
		cod: string,
		count: number,
		list: {
			id: number,
			name: string,
			coord: {
				lat: number,
				lon: number
			},
			main: {
				temp: number,
				feels_like: number,
				temp_min: number,
				temp_max: number,
				pressure: number,
				humidity: number
			},
			dt: number,
			wind: {
				speed: number,
				deg: number
			},
			sys: {
				country: string
			},
			rain?: {
				'1h': number
			},
			snow?: {
				'1h': number
			},
			clouds: {
				all: number
			},
			weather: {
				id: number,
				main: string,
				description: string,
				icon: string
			}[]
		}[]
	}
	interface openweathermapData {
		lat: number,
		lon: number,
		timezone: string,
		timezone_offset: number,
		current: {
			dt: number,
			sunrise: number,
			sunset: number,
			temp: number,
			feels_like: number,
			pressure: number,
			humidity: number,
			dew_point: number,
			clouds: number,
			uvi: number,
			visibility: number,
			wind_speed: number,
			wind_gust?: number,
			wind_deg: number,
			rain?: {
				'1h': number
			},
			snow?: {
				'1h': number
			},
			weather: {
				id: number,
				main: string,
				description: string,
				icon: string
			}[]
		},
		minutely: {
			dt: number,
			precipitation: number
		}[],
		hourly: {
			dt: number,
			temp: number,
			feels_like: number,
			pressure: number,
			humidity: number,
			dew_point: number,
			uvi: number,
			clouds: number,
			visibility: number,
			wind_speed: number,
			wind_gust?: number,
			wind_deg: number,
			pop: number,
			rain?: {
				'1h': number
			},
			snow?: {
				'1h': number
			},
			weather: {
				id: number,
				main: string,
				description: string,
				icon: string
			}[]
		}[],
		daily: {
			dt: number,
			sunrise: number,
			sunset: number,
			moonrise: number,
			moonset: number,
			moon_phase: number,
			temp: {
				morn: number,
				day: number,
				eve: number,
				night: number,
				min: number,
				max: number
			},
			feels_like: {
				morn: number,
				day: number,
				eve: number,
				night: number
			},
			pressure: number,
			humidity: number,
			dew_point: number,
			wind_speed: number,
			wind_gust?: number,
			wind_deg: number,
			clouds: number,
			uvi: number,
			pop: number,
			rain?: number,
			snow?: number,
			weather: {
				id: number,
				main: string,
				description: string,
				icon: string
			}[]
		}[],
		alerts?: {
			sender_name: string,
			event: string,
			start: number,
			end: number,
			description: string,
			tags: string[]
		}[]
	}
	interface pokemonSprites {
		back_default: string,
		back_female: string?,
		back_shiny: string,
		back_shiny_female: string?,
		front_default: string,
		front_female: string?,
		front_shiny: string,
		front_shiny_female: string?,
		other: {
			"official-artwork": {
				front_shiny: string,
				front_default: string
			}
		}
	}
	interface displayPokemonInfo {
		pokemonInfo: {
			name: string,
			id: number,
			base_happiness: number,
			gender_rate: number,
			generation_id: number,
			growth_rate_id: number,
			capture_rate: number,
			evolution_chain_id: number,
			evolves_from_species_id: number | null,
			pokemon_v2_pokemons: {
				name: string,
				base_experience: number,
				id: number,
				height: number,
				weight: number,
				pokemon_v2_pokemonstats: {
					base_stat: number,
					effort: number,
					id: number,
					pokemon_v2_stat: {
						name: string
					}
				}[],
				pokemon_v2_pokemontypes: {
					pokemon_v2_type: {
						name: string
					}
				}[],
				pokemon_v2_pokemonsprites: {
					sprites: pokemonSprites
				}[],
				pokemon_v2_pokemonabilities: {
					is_hidden: boolean,
					pokemon_v2_ability: {
						name: string,
						pokemon_v2_abilityeffecttexts: {
							short_effect: string
						}[]
					}
				}[]
			}[],
			pokemon_v2_pokemonspeciesflavortexts: {
				flavor_text: string
			}[],
			pokemon_v2_pokemonegggroups: {
				pokemon_v2_egggroup: {
					pokemon_v2_egggroupnames: {
						name: string
					}[]
				}
			}[],
			pokemon_v2_growthrate: {
				name: string
			}
		}[]
	}
	interface pokemonEvolution {
		pokemon_v2_pokemonsprites: {
			sprites: pokemonSprites
		}[],
		pokemon_v2_pokemon: {
			name: string,
			id: number,
			pokemon_v2_pokemontypes: {
				pokemon_v2_type: {
					name: string
				}
			}[]
		}[]
	}
	interface pokemonEvolutions {
		pokemon_v2_pokemonspecies: {
			name: string,
			id: number,
			pokemon_v2_pokemons: {
				pokemon_v2_pokemontypes: {
					pokemon_v2_type: {
						name: string
					}
				}[]
			}[]
		}[]
	}
	interface pokemonTypes {
		pokemon_v2_pokemon: {
			id: number,
			name: string,
			pokemon_v2_pokemontypes: {
				pokemon_v2_type: {
					name: string
				}
			}[]
		}[]
	}
	interface pokemonGenerations {
		pokemon_v2_pokemon: {
			id: number,
			name: string,
			pokemon_v2_pokemontypes: {
				pokemon_v2_type: {
					name: string
				}
			}[]
		}[]
	}
	interface pokemonList {
		pokemon_v2_pokemon: {
			name: string,
			pokemon_species_id: number
		}[]
	}
	interface catchInfo {
		capture_rate: number,
		pokemon_v2_pokemons: {
			weight: number,
			pokemon_v2_pokemonstats: {
				base_stat: number,
				pokemon_v2_stat: {
					name: string
				}
			}[],
			pokemon_v2_pokemontypes: {
				pokemon_v2_type: {
					name: string
				}
			}[]
		}[]
	}
	interface pokemonInfo {
		pokemon_v2_pokemonspecies: {
			catchInfo
		}[]
	}
	interface plannerData {
		context_type: string,
		course_id: number,
		plannable_id: number,
		planner_override: null,
		plannable_type: string,
		new_activity: boolean,
		submissions: {
			submitted: boolean,
			excused: boolean,
			graded: boolean,
			posted_at: string,
			late: boolean,
			missing: boolean,
			needs_grading: boolean,
			has_feedback: boolean,
			redo_request: boolean
		},
		plannable_date: string,
		plannable: {
			id: number,
			title: string,
			created_at: string,
			updated_at: string,
			points_possible: number,
			due_at?: string
		},
		html_url: string,
		context_name: string,
		context_image: string
	}
	interface calendarData {
		dueToday: number
		toDo: {
			title: string
			dueDate: string
			url: string
		}[]
	}
	declare module "*&imagetools" {
		/**
		 * actual types
		 * - code https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/output-formats.ts
		 * - docs https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/getting-started.md#metadata
		 */
		const out: string
		export default out
	}
}

export {}