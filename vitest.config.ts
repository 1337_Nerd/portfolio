import path from 'path'
import { defineConfig } from 'vitest/config'

export default defineConfig({
    test: {
        reporters: ['junit', 'basic'],
        outputFile: './coverage/junit.xml',
        coverage: {
			enabled: true,
            reporter: ['html', 'text-summary', 'text', 'cobertura'],
            provider: 'istanbul',
            include: ['src/lib/catch/calc/*.{js,ts}']
        }
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    }
})
