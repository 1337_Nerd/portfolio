import defaultTheme from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			animation: {
				fadeIn: '.5s ease-in forwards fadein',
				openModal: '.2s ease-in forwards openmodal',
				closeModal: '.2s ease-out forwards closemodal'
			},
			colors: {
				highlight: '#b043d1',
				light: '#fdfdfd',
				dark: '#1d1f28',
				onu: '#e85c16',
				'onu-dark': '#cb4810',
				osu: '#bb0000',
				'osu-dark': '#ff3333',
				border: '#d0d1d2',
				'border-dark': '#4a4b53',
				card: '#ededed',
				'card-dark': '#2b2e3c',
				'border-focus': '#8d8e93',
				dropdown: '#7e7e7e',
				'dropdown-dark': '#3a3e50',
				average: '#e143a4'
			},
			fontFamily: {
				sans: ['"Brandon Grotesque"', '"Brandon Grotesque fallback"', ...defaultTheme.fontFamily.sans],
			},
			screens: {
				'3xl': '1600px',
				'4xl': '2000px',
				'5xl': '2500px',
				'6xl': '3000px'
			},
			keyframes: {
				'slide-left': {
					'0%': { transform: 'translateX(10rem)', opacity: 0 },
					'100%': { transform: 'translateX(0)', opacity: 1 }
				},
				fadeOutRightBig: {
					'0%': { opacity: 1 },
					'to': { opacity: 0, transform: 'translate3d(2000px,0,0)' }
				},
				headShake: {
					'0%': { transform: 'translateX(0)' },
					'6.5%': { transform: 'translateX(-6px) rotateY(-9deg)' },
					'18.5%': { transform: 'translateX(5px) rotateY(7deg)' },
					'31.5%': { transform: 'translateX(-3px) rotateY(-5deg)' },
					'43.5%': { transform: 'translateX(2px) rotateY(3deg)' },
					'50%': { transform: 'translateX(0)' }
				},
				fadein: {
					'0%': { opacity: 0.2 },
					'100%': { opacity: 0.9 }
				},
				openmodal: {
					'0%': { opacity: 0 },
					'100%': { opacity: 1, transform: 'translateY(-1rem)' }
				},
				closemodal: {
					'0%': { opacity: 1, transform: 'translateY(-1rem)' },
					'100%': { opacity: 0, transform: 'translateY(0)' }
				}
			}
		}
	},
	plugins: [
		require('@tailwindcss/typography')
	],
}