import adapter from '@sveltejs/adapter-cloudflare';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';
const style = process.env.npm_lifecycle_event === 'dev' ? ["'self'", "'unsafe-inline'"] : ["'self'", "sha256-ESwzs/pwk6xVxIZH3YCxHKVRV9YGR02+QzCza+PCibs=", "sha256-QCOsFZ7I7BDz3DNWoCvMpezU5bOakMTXPTl1BJwK5hA=", "data:"]

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: [vitePreprocess()],

	kit: {
		// adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
		// If your environment is not supported or you settled on a specific environment, switch out the adapter.
		// See https://kit.svelte.dev/docs/adapters for more information about adapters.
		adapter: adapter({
			platformProxy: true
		}),
		csp: {
			directives: {
				'default-src': ["'none'"],
				'script-src': ["'self'", "ajax.cloudflare.com", "https://www.googletagmanager.com", "static.cloudflareinsights.com", "sha256-mNlRemuJXO4/ndZLitC8YDGSoZRTFL56qT9V8ML3wtc=", "sha256-Yc+P8NAjn5lRF9jsk3MfjMhnhSdHR6V9P2sD6sCQpFM=", "https://umami.joshuastock.net"],
				'style-src': style,
				'object-src': ["'none'"],
				'base-uri': ["'self'"],
				'connect-src': ["'self'", "https://beta.pokeapi.co", "https://www.google-analytics.com", "cloudflareinsights.com", "https://umami.joshuastock.net"],
				'font-src': ["'self'"],
				'frame-src': ["'self'"],
				'img-src': ["'self'", "data:", "https://raw.githubusercontent.com", "https://www.googletagmanager.com"],
				'manifest-src': ["'self'", "data:"],
				'media-src': ["'self'"],
				'worker-src': ["'self'"],
				'form-action': ["'self'", "startpage.com"],
				'frame-ancestors': ["'none'"],
				'trusted-types': ["google-tag-manager", "typed-js"],
			}
		}
	}
};

export default config;
