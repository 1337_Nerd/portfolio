# Josh Stock's Portfolio Site
This is my portfolio site that showcases some of the projects I've worked on and provides information about me. It also includes some of my projects that I've linked from there in the same repo.

## Technology Stack
The portfolio site is built using Svelte, Tailwind, and some assorted libraries. The notable ones are sharp for image optimization and fast-fuzzy for fuzzy searching.

## Installation
Getting started is easy! Simply clone the repository using the following commands:

```
git clone https://gitlab.com/1337_Nerd/portfolio.git
npm i
npm run dev -- --open
```

## Project Goals
The goal of my portfolio site is to showcase my skills and abilities to potential employers. My hope is to attract clients who are seeking an experienced professional with a proven track record of success. You'll find my resume included in the portfolio site for easy reference.

## Contact Information
If you have any questions or would like to learn more about my services, please don't hesitate to get in touch with me. You can reach me directly via email at [josh.stock@joshuastock.net](mailto:josh.stock@joshuastock.net) or through LinkedIn at [https://www.linkedin.com/in/josh-stock](https://www.linkedin.com/in/josh-stock).

## Where You Can Find It
You can find my site at [https://joshuastock.net](https://joshuastock.net), with the pages for my [startpage](https://joshuastock.net/startpage), my [Pokémon catch rate calculator](https://joshuastock.net/catch), and my (work in progress) [Pokédex](https://joshuastock.net/pokedex)

![coverage](https://gitlab.com/1337_Nerd/portfolio/badges/main/coverage.svg?job=test)